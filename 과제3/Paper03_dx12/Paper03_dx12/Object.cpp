#include "stdafx.h"
#include "Object.h"
#include "Shader.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CTexture::CTexture(int nTextures, UINT nTextureType, int nSamplers)
{
    m_nTextureType = nTextureType;
    m_nTextures = nTextures;
    if (m_nTextures > 0)
    {
        //CTexture는 SRV정보를 가지고 있는 배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_pRootArgumentInfos = new SRVROOTARGUMENTINFO[m_nTextures];
        //CTexture는 텍스쳐리소스업로드버퍼의 포인터배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_ppd3dTextureUploadBuffers = new ID3D12Resource*[m_nTextures];
        //CTexture는 텍스쳐정보의 포인터배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_ppd3dTextures = new ID3D12Resource*[m_nTextures];
    }

    m_nSamplers = nSamplers;
    if (m_nSamplers > 0) m_pd3dSamplerGpuDescriptorHandles = new D3D12_GPU_DESCRIPTOR_HANDLE[m_nSamplers];
}

CTexture::~CTexture()
{
    if (m_ppd3dTextures)
    {
        for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextures[i]) m_ppd3dTextures[i]->Release();
    }

    if (m_pRootArgumentInfos)
    {
        delete[] m_pRootArgumentInfos;
    }

    if (m_pd3dSamplerGpuDescriptorHandles) delete[] m_pd3dSamplerGpuDescriptorHandles;
}

void CTexture::SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSrvGpuDescriptorHandle)
{
    m_pRootArgumentInfos[nIndex].m_nRootParameterIndex = nRootParameterIndex;
    m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle = d3dSrvGpuDescriptorHandle;
}

void CTexture::SetSampler(int nIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSamplerGpuDescriptorHandle)
{
    m_pd3dSamplerGpuDescriptorHandles[nIndex] = d3dSamplerGpuDescriptorHandle;
}

void CTexture::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    if (m_nTextureType == RESOURCE_TEXTURE2D_ARRAY)
    {
        pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[0].m_nRootParameterIndex, m_pRootArgumentInfos[0].m_d3dSrvGpuDescriptorHandle);
    }
    else
    {
        for (int i = 0; i < m_nTextures; i++)
        {
            pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[i].m_nRootParameterIndex, m_pRootArgumentInfos[i].m_d3dSrvGpuDescriptorHandle);
        }
    }
}

void CTexture::UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, int nIndex)
{
    pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[nIndex].m_nRootParameterIndex, m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle);
}

void CTexture::ReleaseShaderVariables()
{
}

void CTexture::ReleaseUploadBuffers()
{
    if (m_ppd3dTextureUploadBuffers)
    {
        for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextureUploadBuffers[i]) m_ppd3dTextureUploadBuffers[i]->Release();
        delete[] m_ppd3dTextureUploadBuffers;
        m_ppd3dTextureUploadBuffers = NULL;
    }
}

void CTexture::LoadTextureFromFile(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, wchar_t *pszFileName, UINT nIndex)
{
    //CTexture는 텍스쳐(들)를 가지고 있다.
    m_ppd3dTextures[nIndex] = ::CreateTextureResourceFromFile(pd3dDevice, pd3dCommandList, pszFileName, &m_ppd3dTextureUploadBuffers[nIndex], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

CMaterial::CMaterial()
{
    m_xmf4Albedo = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);
}

CMaterial::~CMaterial()
{
    if (m_pShader)
    {
        m_pShader->ReleaseShaderVariables();
        m_pShader->Release();
    }
}

void CMaterial::SetShader(CShader *pShader)
{
    if (m_pShader) m_pShader->Release();
    m_pShader = pShader;
    if (m_pShader) m_pShader->AddRef();
}

void CMaterial::SetTexture(CTexture *pTexture)
{
    if (m_pTexture) m_pTexture->Release();
    m_pTexture = pTexture;
    if (m_pTexture) m_pTexture->AddRef();
}

void CMaterial::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    if (m_pTexture) m_pTexture->UpdateShaderVariables(pd3dCommandList);
}

void CMaterial::ReleaseShaderVariables()
{
    if (m_pShader) m_pShader->ReleaseShaderVariables();
    if (m_pTexture) m_pTexture->ReleaseShaderVariables();
}

void CMaterial::ReleaseUploadBuffers()
{
    if (m_pTexture) m_pTexture->ReleaseUploadBuffers();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

CGameObject::CGameObject(int nMeshes) 
{
    m_xmf4x4ToParentTransform = Matrix4x4::Identity();
    m_xmf4x4World = Matrix4x4::Identity();

    m_nMeshes = nMeshes; 
    m_ppMeshes = NULL; 
    if (m_nMeshes > 0) 
    { 
        m_ppMeshes = new CMesh*[m_nMeshes]; 
        for (int i = 0; i < m_nMeshes; i++) 
            m_ppMeshes[i] = NULL; 
    }
}

CGameObject::~CGameObject()
{

    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->Release(); 
            m_ppMeshes[i] = NULL; 
        } 
        delete[] m_ppMeshes;
    }
    
    if (m_pShader)
    { 
        m_pShader->ReleaseShaderVariables(); 
        m_pShader->Release(); 
    }

    if (m_pMaterial) m_pMaterial->Release();

    if (m_pSibling) delete m_pSibling;
    if (m_pChild) delete m_pChild;
}

void CGameObject::ResizeMeshes(int nMeshes)
{
    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++)
        {
            if (m_ppMeshes[i]) m_ppMeshes[i]->Release();
            m_ppMeshes[i] = NULL;
        }
        delete[] m_ppMeshes;
        m_ppMeshes = NULL;
    }

    m_nMeshes = nMeshes;
    m_ppMeshes = NULL;
    if (m_nMeshes > 0)
    {
        m_ppMeshes = new CMesh*[m_nMeshes];
        for (int i = 0; i < m_nMeshes; i++)	m_ppMeshes[i] = NULL;
    }
}

void CGameObject::SetChild(CGameObject *pChild)
{
    if (m_pChild)
    {
        if (pChild) pChild->m_pSibling = m_pChild->m_pSibling;
        m_pChild->m_pSibling = pChild;
    }
    else
    {
        m_pChild = pChild;
    }
    if (pChild) pChild->m_pParent = this;
}

void CGameObject::SetShader(CShader *pShader)
{
    //if (m_pShader) m_pShader->Release();
    //m_pShader = pShader;
    //if (m_pShader) m_pShader->AddRef();

    //이제부터는 메테리얼이 쉐이더를 갖는다.
    //만약 메테리얼이 없으면 바로 메모리할당한다음 쉐이더를 Set한다.
    if (!m_pMaterial)
    {
        CMaterial *pMaterial = new CMaterial();
        SetMaterial(pMaterial);
    }
    if (m_pMaterial) m_pMaterial->SetShader(pShader);
}

void CGameObject::SetMesh(int nIndex, CMesh *pMesh) 
{ 
    if (m_ppMeshes) 
    { 
        if (m_ppMeshes[nIndex]) m_ppMeshes[nIndex]->Release(); 
        m_ppMeshes[nIndex] = pMesh; 
        if (pMesh) pMesh->AddRef(); 
    } 
}

void CGameObject::SetMaterial(CMaterial *pMaterial)
{
    if (m_pMaterial) m_pMaterial->Release();
    m_pMaterial = pMaterial;
    if (m_pMaterial) m_pMaterial->AddRef();
}

void CGameObject::SetMaterial(UINT nReflection)
{
    if (!m_pMaterial) m_pMaterial = new CMaterial();
    m_pMaterial->m_nReflection = nReflection;
}

void CGameObject::ReleaseUploadBuffers()
{
    if (m_ppMeshes) 
    { 
        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->ReleaseUploadBuffers(); 
        } 
    }

    if (m_pMaterial) m_pMaterial->ReleaseUploadBuffers();

    if (m_pSibling) m_pSibling->ReleaseUploadBuffers();
    if (m_pChild) m_pChild->ReleaseUploadBuffers();
}

void CGameObject::UpdateTransform(XMFLOAT4X4 *pxmf4x4Parent)
{
    m_xmf4x4World = (pxmf4x4Parent) ? Matrix4x4::Multiply(m_xmf4x4ToParentTransform, *pxmf4x4Parent) : m_xmf4x4ToParentTransform;

    if (m_pSibling) m_pSibling->UpdateTransform(pxmf4x4Parent);
    if (m_pChild) m_pChild->UpdateTransform(&m_xmf4x4World);
}

void CGameObject::Animate(float fTimeElapsed, CCamera * pCamera) 
{ 
    if (m_pSibling) m_pSibling->Animate(fTimeElapsed);
    if (m_pChild) m_pChild->Animate(fTimeElapsed);
}

void CGameObject::OnPrepareRender() { }

void CGameObject::Rotate(XMFLOAT3 *pxmf3Axis, float fAngle) 
{
    XMMATRIX mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(pxmf3Axis), XMConvertToRadians(fAngle)); 
    m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

ID3D12Resource * CGameObject::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{ 
    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    m_pd3dcbGameObject = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
    m_pd3dcbGameObject->Map(0, NULL, (void **)&m_pcbMappedGameObject);

    return(m_pd3dcbGameObject);
}

void CGameObject::ReleaseShaderVariables() 
{ 
}

void CGameObject::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList) 
{
    XMStoreFloat4x4(&m_pcbMappedGameObject->m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
    if(m_pMaterial) m_pcbMappedGameObject->m_nMaterial = m_pMaterial->m_nReflection;
}

void CGameObject::SetDescriptorTable(ID3D12GraphicsCommandList * pd3dCommandList)
{
    pd3dCommandList->SetGraphicsRootDescriptorTable(0, m_d3dCbvGPUDescriptorHandle);
}

//1. 인스턴싱을 사용하지 않는방법
void CGameObject::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera) 
{  
    if (!m_bHellfireRender) return;

    OnPrepareRender();

    //이제부터 메테리얼이 쉐이더를 갖는다.
    if (m_pMaterial)
    {
        if (m_pMaterial->m_pShader)
        {
            m_pMaterial->m_pShader->Render(pd3dCommandList, pCamera);
            m_pMaterial->m_pShader->UpdateShaderVariables(pd3dCommandList);
            m_pMaterial->m_pShader->CShader::SetDescriptorHeaps(pd3dCommandList);
            //CGameObject::UpdateShaderVariables는 객체를 하나 그릴때만 사용된다.
            UpdateShaderVariables(pd3dCommandList);
        }

        if (m_pMaterial->m_pTexture)
        {
            m_pMaterial->m_pTexture->UpdateShaderVariables(pd3dCommandList);
        }
    }

    //게임 객체가 포함하는 모든 메쉬를 렌더링한다. 
    if (m_ppMeshes && (m_nMeshes > 0 )) 
    { 
        //현재 터레인과 구오브젝트들은 동일한 시그너쳐를 사용하고 있다.
        //그렇기 때문에 두 객체가 렌더링 될때마다 호출한다.
        //루트파라미터0번 인덱스에 있는 Descriptortable을 Set한다.
        //주의할점은 DescriptorTable은 DescriptorHeap이 우선적으로 Set되어있어야한다.
        CGameObject::SetDescriptorTable(pd3dCommandList);

        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList); 
        } 
    } 

    if (m_pSibling) m_pSibling->Render(pd3dCommandList, pCamera);
    if (m_pChild) m_pChild->Render(pd3dCommandList, pCamera);
}

//2. 인스턴싱을 사용하는 방법
//인스턴싱 정점 버퍼 뷰를 사용하여 메쉬를 렌더링한다. 
void CGameObject::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera, UINT nInstances, D3D12_VERTEX_BUFFER_VIEW d3dInstancingBufferView)
{
    OnPrepareRender();
    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++)
        {
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList, nInstances, d3dInstancingBufferView);
        }
    }
}

void CGameObject::SetPosition(float x, float y, float z) 
{ 
    m_xmf4x4World._41 = x;
    m_xmf4x4World._42 = y;
    m_xmf4x4World._43 = z;

    m_xmf4x4ToParentTransform._41 = x;
    m_xmf4x4ToParentTransform._42 = y;
    m_xmf4x4ToParentTransform._43 = z;
}

void CGameObject::SetPosition(XMFLOAT3 xmf3Position) 
{ 
    SetPosition(xmf3Position.x, xmf3Position.y, xmf3Position.z); 
}

void CGameObject::SetLocalPosition(XMFLOAT3 xmf3Position)
{
    XMMATRIX mtxTranslation = XMMatrixTranslation(xmf3Position.x, xmf3Position.y, xmf3Position.z);
    m_xmf4x4ToParentTransform = Matrix4x4::Multiply(m_xmf4x4ToParentTransform, mtxTranslation);
}

void CGameObject::SetScale(float x, float y, float z)
{
    XMMATRIX mtxScale = XMMatrixScaling(x, y, z);
    m_xmf4x4ToParentTransform = Matrix4x4::Multiply(mtxScale, m_xmf4x4ToParentTransform);
}

XMFLOAT3 CGameObject::GetPosition() 
{ 
    return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43)); 
}

//게임 객체의 로컬 z-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetLook() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33))); 
}

//게임 객체의 로컬 y-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetUp() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23))); 
}

//게임 객체의 로컬 x-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetRight() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13))); 
}

//게임 객체를 로컬 x-축 방향으로 이동한다. 
void CGameObject::MoveStrafe(float fDistance) 
{
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Right = GetRight(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Right, fDistance); 
    CGameObject::SetPosition(xmf3Position); 
}

//게임 객체를 로컬 y-축 방향으로 이동한다. 
void CGameObject::MoveUp(float fDistance) 
{ 
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Up = GetUp(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Up, fDistance);
    CGameObject::SetPosition(xmf3Position);
}

//게임 객체를 로컬 z-축 방향으로 이동한다. 
void CGameObject::MoveForward(float fDistance) 
{ 
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Look = GetLook(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Look, fDistance); 
    CGameObject::SetPosition(xmf3Position); 
}

//게임 객체를 주어진 각도로 회전한다. 
void CGameObject::Rotate(float fPitch, float fYaw, float fRoll) 
{ 
    XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll)); 
    m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World); 
}

#define _WITH_DEBUG_FRAME_HIERARCHY

void CGameObject::LoadFrameHierarchyFromFile(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature, wifstream& InFile, UINT nFrame)
{
    XMFLOAT3 *pxmf3Positions = NULL, *pxmf3Normals = NULL;
    XMFLOAT2 *pxmf3TextureCoords0 = NULL, *pxmf3TextureCoords1 = NULL;
    UINT *pnIndices = NULL;

    TCHAR pstrMeshName[64] = { '\0' };
    TCHAR pstrAlbedoTextureName[64] = { '\0' };
    TCHAR pstrToken[64] = { '\0' };
    TCHAR pstrDebug[128] = { '\0' };

    XMFLOAT3 xmf3FrameLocalPosition, xmf3FrameLocalRotation, xmf3FrameLocalScale, xmf3FrameScale;
    XMFLOAT4 xmf4FrameLocalQuaternion, xmf4MaterialAlbedo;
    int nVertices = 0, nNormals = 0, nTextureCoords = 0, nIndices = 0;

    for (; ; )
    {
        InFile >> pstrToken;
        if (!InFile) break;

        if (!_tcscmp(pstrToken, _T("FrameName:")))
        {
            InFile >> m_pstrFrameName;

            nVertices = nNormals = nTextureCoords = nIndices = 0;
            xmf4MaterialAlbedo = XMFLOAT4(-1.0f, -1.0f, -1.0f, -1.0f);
            pxmf3Positions = pxmf3Normals = NULL;
            pxmf3TextureCoords0 = pxmf3TextureCoords1 = NULL;
            pstrAlbedoTextureName[0] = '\0';
            pnIndices = NULL;
        }
        else if (!_tcscmp(pstrToken, _T("Transform:")))
        {
            InFile >> xmf3FrameLocalPosition.x >> xmf3FrameLocalPosition.y >> xmf3FrameLocalPosition.z;
            InFile >> xmf3FrameLocalRotation.x >> xmf3FrameLocalRotation.y >> xmf3FrameLocalRotation.z;
            InFile >> xmf4FrameLocalQuaternion.x >> xmf4FrameLocalQuaternion.y >> xmf4FrameLocalQuaternion.z >> xmf4FrameLocalQuaternion.w;
            InFile >> xmf3FrameLocalScale.x >> xmf3FrameLocalScale.y >> xmf3FrameLocalScale.z;
            InFile >> xmf3FrameScale.x >> xmf3FrameScale.y >> xmf3FrameScale.z;
        }
        else if (!_tcscmp(pstrToken, _T("TransformMatrix:")))
        {
            InFile >> m_xmf4x4ToRootTransform._11 >> m_xmf4x4ToRootTransform._12 >> m_xmf4x4ToRootTransform._13 >> m_xmf4x4ToRootTransform._14;
            InFile >> m_xmf4x4ToRootTransform._21 >> m_xmf4x4ToRootTransform._22 >> m_xmf4x4ToRootTransform._23 >> m_xmf4x4ToRootTransform._24;
            InFile >> m_xmf4x4ToRootTransform._31 >> m_xmf4x4ToRootTransform._32 >> m_xmf4x4ToRootTransform._33 >> m_xmf4x4ToRootTransform._34;
            InFile >> m_xmf4x4ToRootTransform._41 >> m_xmf4x4ToRootTransform._42 >> m_xmf4x4ToRootTransform._43 >> m_xmf4x4ToRootTransform._44;

            InFile >> m_xmf4x4ToParentTransform._11 >> m_xmf4x4ToParentTransform._12 >> m_xmf4x4ToParentTransform._13 >> m_xmf4x4ToParentTransform._14;
            InFile >> m_xmf4x4ToParentTransform._21 >> m_xmf4x4ToParentTransform._22 >> m_xmf4x4ToParentTransform._23 >> m_xmf4x4ToParentTransform._24;
            InFile >> m_xmf4x4ToParentTransform._31 >> m_xmf4x4ToParentTransform._32 >> m_xmf4x4ToParentTransform._33 >> m_xmf4x4ToParentTransform._34;
            InFile >> m_xmf4x4ToParentTransform._41 >> m_xmf4x4ToParentTransform._42 >> m_xmf4x4ToParentTransform._43 >> m_xmf4x4ToParentTransform._44;
        }
        else if (!_tcscmp(pstrToken, _T("MeshName:")))
        {
            InFile >> pstrMeshName;
        }
        else if (!_tcscmp(pstrToken, _T("Vertices:")))
        {
            InFile >> nVertices;
            pxmf3Positions = new XMFLOAT3[nVertices];
            for (int i = 0; i < nVertices; i++)
            {
                InFile >> pxmf3Positions[i].x >> pxmf3Positions[i].y >> pxmf3Positions[i].z;
            }
        }
        else if (!_tcscmp(pstrToken, _T("Normals:")))
        {
            InFile >> nNormals;
            pxmf3Normals = new XMFLOAT3[nNormals];
            for (int i = 0; i < nNormals; i++)
            {
                InFile >> pxmf3Normals[i].x >> pxmf3Normals[i].y >> pxmf3Normals[i].z;
            }
        }
        else if (!_tcscmp(pstrToken, _T("TextureCoordinates0:")))
        {
            InFile >> nTextureCoords;
            pxmf3TextureCoords0 = new XMFLOAT2[nTextureCoords];
            for (int i = 0; i < nTextureCoords; i++)
            {
                InFile >> pxmf3TextureCoords0[i].x >> pxmf3TextureCoords0[i].y;
            }
        }
        else if (!_tcscmp(pstrToken, _T("TextureCoordinates1:")))
        {
            InFile >> nTextureCoords;
            pxmf3TextureCoords1 = new XMFLOAT2[nTextureCoords];
            for (int i = 0; i < nTextureCoords; i++)
            {
                InFile >> pxmf3TextureCoords1[i].x >> pxmf3TextureCoords1[i].y;
            }
        }
        else if (!_tcscmp(pstrToken, _T("Indices:")))
        {
            InFile >> nIndices;
            pnIndices = new UINT[nIndices];
            for (int i = 0; i < nIndices; i++)
            {
                InFile >> pnIndices[i];
            }
        }
        else if (!_tcscmp(pstrToken, _T("AlbedoColor:")))
        {
            InFile >> xmf4MaterialAlbedo.x >> xmf4MaterialAlbedo.y >> xmf4MaterialAlbedo.z >> xmf4MaterialAlbedo.w;
        }
        else if (!_tcscmp(pstrToken, _T("AlbedoTextureName:")))
        {
            InFile >> pstrAlbedoTextureName;
        }
        else if (!_tcscmp(pstrToken, _T("Children:")))
        {
            int nChilds = 0;
            InFile >> nChilds;
            if (nChilds > 0)
            {
                for (int i = 0; i < nChilds; i++)
                {
                    CGameObject *pChild = new CGameObject(1);
                    pChild->LoadFrameHierarchyFromFile(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, InFile, nFrame + 1);
                    SetChild(pChild);
#ifdef _WITH_DEBUG_FRAME_HIERARCHY
                    _stprintf_s(pstrDebug, 128, _T("(Frame: %p) (Parent: %p)\n"), pChild, this);
                    OutputDebugString(pstrDebug);
#endif
                }
            }
        }
        else if (!_tcscmp(pstrToken, _T("EndOfFrame")))
        {
            CMesh *pMesh = NULL;
            CMaterial *pMaterial = NULL;
            if ((nNormals > 0) && (nTextureCoords > 0) && (pstrAlbedoTextureName[0] != '\0'))
            {
                if (nVertices > 0) pMesh = new CMeshIlluminatedTextured(pd3dDevice, pd3dCommandList, nVertices, pxmf3Positions, pxmf3Normals, pxmf3TextureCoords0, nIndices, pnIndices);

                TCHAR pstrPathName[128] = { '\0' };
                _tcscpy_s(pstrPathName, 128, _T("../Assets/Model/"));
                _tcscat_s(pstrPathName, 128, pstrAlbedoTextureName);
                _tcscat_s(pstrPathName, 128, _T(".dds"));

                CTexture *pTexture = new CTexture(1, RESOURCE_TEXTURE2D, 0);
                pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, pstrPathName, 0);

                pMaterial = new CMaterial();
                pMaterial->m_xmf4Albedo = xmf4MaterialAlbedo;

                pMaterial->SetTexture(pTexture);

                UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);

                //Lab 프로젝트에 CreateShaderVariables과 반환형이 다르다. 
                ID3D12Resource *pd3dcbResource = CreateShaderVariables(pd3dDevice, pd3dCommandList);

                CIlluminatedTexturedShader *pShader = new CIlluminatedTexturedShader();
                pShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
                pShader->CreateShaderVariables(pd3dDevice, pd3dCommandList);
                pShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 1);
                pShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, pd3dcbResource, ncbElementBytes);
                pShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pTexture, 12, true);

                SetCbvGPUDescriptorHandle(pShader->GetGPUCbvDescriptorStartHandle());

                pMaterial->SetShader(pShader);
            }
            else if (nNormals > 0)
            {
                if (nVertices > 0) pMesh = new CMeshIlluminated(pd3dDevice, pd3dCommandList, nVertices, pxmf3Positions, pxmf3Normals, nIndices, pnIndices);

                pMaterial = new CMaterial();
                pMaterial->m_xmf4Albedo = xmf4MaterialAlbedo;

                UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);

                //Lab 프로젝트에 CreateShaderVariables과 반환형이 다르다. 
                ID3D12Resource *pd3dcbResource = CreateShaderVariables(pd3dDevice, pd3dCommandList);

                CIlluminatedShader * pShader = new CIlluminatedShader();
                pShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
                pShader->CreateShaderVariables(pd3dDevice, pd3dCommandList);
                pShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 0);
                pShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, pd3dcbResource, ncbElementBytes);

                SetCbvGPUDescriptorHandle(pShader->GetGPUCbvDescriptorStartHandle());

                pMaterial->SetShader(pShader);
            }
            else if (nTextureCoords > 0)
            {
                if (nVertices > 0) pMesh = new CMeshTextured(pd3dDevice, pd3dCommandList, nVertices, pxmf3Positions, pxmf3TextureCoords0, nIndices, pnIndices);
            }
            else
            {
                if (nVertices > 0) pMesh = new CMesh(pd3dDevice, pd3dCommandList, nVertices, pxmf3Positions, nIndices, pnIndices);
            }

            if (pMesh)
                SetMesh(0, pMesh);
            else
                ResizeMeshes(0);

            if (pMaterial) SetMaterial(pMaterial);

            if (pxmf3Positions) delete[] pxmf3Positions;
            if (pxmf3Normals) delete[] pxmf3Normals;
            if (pxmf3TextureCoords0) delete[] pxmf3TextureCoords0;
            if (pxmf3TextureCoords1) delete[] pxmf3TextureCoords1;
            if (pnIndices) delete[] pnIndices;

            break;
        }
    }
}

CGameObject *CGameObject::FindFrame(_TCHAR *pstrFrameName)
{
    CGameObject *pFrameObject = NULL;
    if (!_tcsncmp(m_pstrFrameName, pstrFrameName, _tcslen(pstrFrameName))) return(this);

    if (m_pSibling) if (pFrameObject = m_pSibling->FindFrame(pstrFrameName)) return(pFrameObject);
    if (m_pChild) if (pFrameObject = m_pChild->FindFrame(pstrFrameName)) return(pFrameObject);

    return(NULL);
}

void CGameObject::PrintFrameInfo(CGameObject *pGameObject, CGameObject *pParent)
{
    TCHAR pstrDebug[128] = { 0 };

    _stprintf_s(pstrDebug, 128, _T("(Frame: %p) (Parent: %p)\n"), pGameObject, pParent);
    OutputDebugString(pstrDebug);

    if (pGameObject->m_pSibling) PrintFrameInfo(pGameObject->m_pSibling, pParent);
    if (pGameObject->m_pChild) PrintFrameInfo(pGameObject->m_pChild, pGameObject);
}

void CGameObject::LoadGeometryFromFile(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature, TCHAR *pstrFileName)
{
    wifstream InFile(pstrFileName);
    LoadFrameHierarchyFromFile(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, InFile, 0);

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
    TCHAR pstrDebug[128] = { 0 };
    _stprintf_s(pstrDebug, 128, _T("Frame Hierarchy\n"));
    OutputDebugString(pstrDebug);

    PrintFrameInfo(this, NULL);
#endif
}

////////////////////////////////////

CRotatingObject::CRotatingObject(int nMeshes) : CGameObject(nMeshes) 
{ 
    m_xmf3RotationAxis = XMFLOAT3(0.0f, 1.0f, 0.0f); 
    m_fRotationSpeed = 15.0f; 

}

CRotatingObject::~CRotatingObject() 
{ 
}

void CRotatingObject::Animate(float fTimeElapsed, CCamera * pCamera)
{ 
    CGameObject::Rotate(&m_xmf3RotationAxis, m_fRotationSpeed * fTimeElapsed); 
}


//////////////////////////////////////////////////////////////////////////


CRotatingMoveObject::CRotatingMoveObject(int nMeshes) : CRotatingObject(1)
{
    m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_fMovingSpeed = 0.0f;
    m_pTerrainData = NULL;
}

CRotatingMoveObject::~CRotatingMoveObject()
{
    delete m_pTerrainData;
}

void CRotatingMoveObject::SetDirection(XMFLOAT3 xmf3Direction)
{
    m_xmf3Direction = xmf3Direction;
    m_xmf3Direction = Vector3::Normalize(m_xmf3Direction);
}

void CRotatingMoveObject::SetMovingSpeed(float fMovingSpeed)
{
    m_fMovingSpeed = fMovingSpeed;
};

void CRotatingMoveObject::SetTerrainData(CHeightMapTerrain * pTerrain)
{
    m_pTerrainData = pTerrain;
}

XMFLOAT3 CRotatingMoveObject::GetDirection(void)
{
    return m_xmf3Direction;
}

void CRotatingMoveObject::Animate(float fTimeElapsed, CCamera * pCamera)
{
    assert(m_pTerrainData != NULL && "Terrain데이터가 없습니다");

    //속도 x 시간
    Move(m_fMovingSpeed * fTimeElapsed);
    CGameObject::Rotate(&m_xmf3RotationAxis, m_fRotationSpeed * fTimeElapsed);   
}

void CRotatingMoveObject::Move(float fDistance)
{
    //객체가 터레인 밖으로 나가면 방향벡터가 반사된다.
    CheckTerrainOutSide();

    //객체의 높이값을 터레인에 맞춰서 조정한다.
    UpdateByTerrainData();
   
    XMFLOAT3 xmf3Position = Vector3::Add(GetPosition(), m_xmf3Direction, fDistance);
    CGameObject::SetPosition(xmf3Position);
}

void CRotatingMoveObject::UpdateByTerrainData(void)
{
    XMFLOAT3 xmf3Position = GetPosition();

    float fHeight = m_pTerrainData->GetHeight(xmf3Position.x, xmf3Position.z) + 10.0f;

    //객체y 값이 터레인 y값보다 작으면 올린다.
    if (xmf3Position.y < fHeight)
    {
        xmf3Position.y = fHeight;
        SetPosition(xmf3Position);
    }
    //객체y 값이 터레인 y값보다 크면 내린다.
    else if (xmf3Position.y > fHeight)
    {
        xmf3Position.y = fHeight;
        SetPosition(xmf3Position);
    }
}

XMFLOAT3 CRotatingMoveObject::CheckTerrainOutSide(void)
{
    XMFLOAT3 xmf3Position = GetPosition();
    
    XMFLOAT3 xmf3Normal;
    if (xmf3Position.x > m_pTerrainData->GetWidth()) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(-1.0f, 0.0f, 0.0f));
    if (xmf3Position.x < 0) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(1.0f, 0.0f, 0.0f));
    if (xmf3Position.z > m_pTerrainData->GetLength()) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(0.0f, 0.0f, -1.0f));
    if (xmf3Position.z < 0) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(0.0f, 0.0f, 1.0f));

    //터레인 밖으로 나가지 않으면 기존 방향벡터를 반환
    return m_xmf3Direction;
}


///////////////////////////////////////////////////////////////////////


CHeightMapTerrain::CHeightMapTerrain(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, 
    ID3D12RootSignature *pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength, 
    int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color) : CGameObject() 
{ 
    //지형에 사용할 높이 맵의 가로, 세로의 크기이다. 
    m_nWidth = nWidth; 
    m_nLength = nLength;
    
    /*지형 객체는 격자 메쉬들의 배열로 만들 것이다. nBlockWidth, nBlockLength는 격자 메쉬 하나의 가로, 세로 크 기이다. cxQuadsPerBlock, czQuadsPerBlock은 격자 메쉬의 가로 방향과 세로 방향 사각형의 개수이다.*/ 
    int cxQuadsPerBlock = nBlockWidth - 1; 
    int czQuadsPerBlock = nBlockLength - 1;
    
    //xmf3Scale는 지형을 실제로 몇 배 확대할 것인가를 나타낸다. 
    m_xmf3Scale = xmf3Scale;
    
    //지형에 사용할 높이 맵을 생성한다. 
    m_pHeightMapImage = new CHeightMapImage(pFileName, nWidth, nLength, xmf3Scale);
    
    //지형에서 가로 방향, 세로 방향으로 격자 메쉬가 몇 개가 있는 가를 나타낸다. 
    long cxBlocks = (m_nWidth - 1) / cxQuadsPerBlock; 
    long czBlocks = (m_nLength - 1) / czQuadsPerBlock;

    //지형 전체를 표현하기 위한 격자 메쉬의 개수이다. 
    m_nMeshes = cxBlocks * czBlocks; 
    
    //지형 전체를 표현하기 위한 격자 메쉬에 대한 포인터 배열을 생성한다. 
    m_ppMeshes = new CMesh*[m_nMeshes]; 
    for (int i = 0; i < m_nMeshes; i++)
        m_ppMeshes[i] = NULL;

    CHeightMapGridMesh *pHeightMapGridMesh = NULL; 
    for (int z = 0, zStart = 0; z < czBlocks; z++) 
    {
        for (int x = 0, xStart = 0; x < cxBlocks; x++) 
        { 
            //지형의 일부분을 나타내는 격자 메쉬의 시작 위치(좌표)이다. 
            xStart = x * (nBlockWidth - 1); 
            zStart = z * (nBlockLength - 1); 
            //지형의 일부분을 나타내는 격자 메쉬를 생성하여 지형 메쉬에 저장한다. 
            pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, pd3dCommandList, xStart, zStart, 
                nBlockWidth, nBlockLength, xmf3Scale, xmf4Color, m_pHeightMapImage); 
            SetMesh(x + (z*cxBlocks), pHeightMapGridMesh); 
        } 
    }
                     
    //터레인정보를 담고 있는 리소스를 만들고 Map함수를 통해서 리소스 주소값을 가져온다.
    ID3D12Resource *pd3dcbResource = CreateShaderVariables(pd3dDevice, pd3dCommandList);

    CTerrainShader * pTerrainShader = new CTerrainShader();
    pTerrainShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);

    //디스크립터힙을 만들고, cpu,gpu 디스크립터 핸들을 얻는다. 여기서는 cbv한개와 srv2개를 담을 디스크립터힙를 생성한다.
    pTerrainShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 3);

    //디스크립터힙을 만들었으니 힙안에다가 view를 만든다. 
    //힙안에 cbv 1개 생성
    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    pTerrainShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, pd3dcbResource, ncbElementBytes);
    
    CTexture *pTerrainTexture = new CTexture(3, RESOURCE_TEXTURE2D, 0);
    pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/Terrain/Base_Texture.dds", 0);
    pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/Terrain/Detail_Texture_7.dds", 1);
    pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/Terrain/terrain_normals.dds", 2);

    //힙안에 srv 2개 생성
    pTerrainShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pTerrainTexture, 6, true);
  
    CMaterial * pTerrainMaterial = new CMaterial();

    //머테리얼은 재질(빛을 반사하는 성질)을 가지고 있다.
    pTerrainMaterial->SetReflection(1);

    //머테리얼은 텍스쳐를 가지고 있다.
    pTerrainMaterial->SetTexture(pTerrainTexture);

    //머테리얼은 쉐이더도 가지고 있다.
    pTerrainMaterial->SetShader(pTerrainShader);

    //터레인오브젝트는 재질을 가지고있다.
    //SetShader를 호출하려면 먼저 메테리얼이 Set 되있어야할것이다. 왜냐? 상위 개념이기때문에
    //고로 SetMaterial이 SetShader보다 먼저 호출되어야함.
    //결국 터레인 오브젝트는 머테리얼이라는 변수를 가지고 있는데 
    //그 머테리얼은 텍스쳐와 재질(빛을 반사하는 성질), 쉐이더를 가지고 있다.
    SetMaterial(pTerrainMaterial);

    //터레인오브젝트는 힙의 gpu첫주소값을 쉐이더로부터 얻는다.
    SetCbvGPUDescriptorHandle(pTerrainShader->GetGPUCbvDescriptorStartHandle());

    //여기서 pTerrainShader 메모리를 해제하면 안될것이다. 왜냐? ExecuteCommandList가 아직 호출이 안됐기때문에.
}

CHeightMapTerrain::~CHeightMapTerrain(void) 
{ 
    if (m_pHeightMapImage) delete m_pHeightMapImage; 
}

void CHeightMapTerrain::Render(ID3D12GraphicsCommandList * pd3dCommandList, CCamera * pCamera)
{
    CGameObject::Render(pd3dCommandList, pCamera);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
CSkyBox::CSkyBox(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature) : CGameObject(6)
{
    CTexturedRectMesh *pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, +10.0f);
    SetMesh(0, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, -10.0f);
    SetMesh(1, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 0.0f, 20.0f, 20.0f, -10.0f, 0.0f, 0.0f);
    SetMesh(2, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 0.0f, 20.0f, 20.0f, +10.0f, 0.0f, 0.0f);
    SetMesh(3, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 0.0f, 20.0f, 0.0f, +10.0f, 0.0f);
    SetMesh(4, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 0.0f, 20.0f, 0.0f, -10.0f, 0.0f);
    SetMesh(5, pSkyBoxMesh);

    //아래 함수를 통해서 스카이박스정보를 저장할수있는 리소스를 만들고 Map함수를 통해서 리소스 주소값을 가져온다.
    ID3D12Resource *pd3dcbResource = CreateShaderVariables(pd3dDevice, pd3dCommandList);

    CTexture *pSkyBoxTexture = new CTexture(6, RESOURCE_TEXTURE2D, 0);

    //텍스쳐(dds)를 불러온다
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Front_0.dds", 0);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Back_0.dds", 1);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Left_0.dds", 2);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Right_0.dds", 3);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Top_0.dds", 4);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Bottom_0.dds", 5);

    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

    //스카이박스는 쉐이더를 가지고 있다.
    CSkyBoxShader *pSkyBoxShader = new CSkyBoxShader();
    pSkyBoxShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
    //pSkyBoxShader->CreateShaderVariables(pd3dDevice, pd3dCommandList);
    //스카이박스쉐이더는 cbv,srv를 binding하는 descriptorHeap을 생성한다. cbv 1개, srv 6개
    pSkyBoxShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 6);
    //위에서 만든 descriptorHeap에 cbv1개를 Set한다.
    pSkyBoxShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, pd3dcbResource, ncbElementBytes);
    //위에서 만든 descriptorHeap에 srv(6개)를 Set한다. 마지막인자가 False는 하나의 루트파라미터인덱스를 사용한다는 의미
    pSkyBoxShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pSkyBoxTexture, 2, false);

    //재질은 텍스쳐를 가지고 있다.
    CMaterial *pSkyBoxMaterial = new CMaterial();

    pSkyBoxMaterial->SetTexture(pSkyBoxTexture);

    pSkyBoxMaterial->SetShader(pSkyBoxShader);

    //여기서 만약 재절(빛을 반사하는성질)을 따로 Set해주지않으면 디폴트 재질이 생성되서 Set된다?

    //스카이박스 오브젝트는 머테리얼(m_pMaterial)을 가지고 있다.
    //그 머테리얼은 재질(빛을 반사하는 성질)과 텍스쳐와 쉐이더를 가지고 있다.
    SetMaterial(pSkyBoxMaterial);

    //스카이박스 오브젝트는 힙의 gpu첫주소값을 쉐이더로부터 얻는다.
    SetCbvGPUDescriptorHandle(pSkyBoxShader->GetGPUCbvDescriptorStartHandle());
}

CSkyBox::~CSkyBox()
{
}

void CSkyBox::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
    XMFLOAT3 xmf3CameraPos = pCamera->GetPosition();
    SetPosition(xmf3CameraPos.x, xmf3CameraPos.y, xmf3CameraPos.z);

    OnPrepareRender();

    if (m_pMaterial)
    {
        if (m_pMaterial->m_pShader)
        {
            m_pMaterial->m_pShader->Render(pd3dCommandList, pCamera);
            m_pMaterial->m_pShader->UpdateShaderVariables(pd3dCommandList);
            m_pMaterial->m_pShader->CShader::SetDescriptorHeaps(pd3dCommandList);
            UpdateShaderVariables(pd3dCommandList);
        }
    }

    CGameObject::SetDescriptorTable(pd3dCommandList);

    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++)
        {
            if (m_pMaterial)
            {
                if (m_pMaterial->m_pTexture) m_pMaterial->m_pTexture->UpdateShaderVariable(pd3dCommandList, i);
            }
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CBillBoardObject::CBillBoardObject() : CGameObject(1)
{
}

CBillBoardObject::~CBillBoardObject()
{
}

void CBillBoardObject::Animate(float fTimeElapsed, CCamera * pCamera)
{
    XMFLOAT3 xmf3CameraPosition = pCamera->GetPosition();
    SetLookAt(xmf3CameraPosition);
}

void CBillBoardObject::SetLookAt(XMFLOAT3 & xmf3Target)
{
    XMFLOAT3 xmf3Up(0.0f, 1.0f, 0.0f);
    XMFLOAT3 xmf3Position(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43);
    XMFLOAT3 xmf3Look = Vector3::Subtract(xmf3Target, xmf3Position);
    xmf3Look = Vector3::Normalize(xmf3Look);
    XMFLOAT3 xmf3Right = Vector3::CrossProduct(xmf3Up, xmf3Look, true);
    m_xmf4x4World._11 = xmf3Right.x; m_xmf4x4World._12 = xmf3Right.y; m_xmf4x4World._13 = xmf3Right.z;
    m_xmf4x4World._21 = xmf3Up.x, m_xmf4x4World._22 = xmf3Up.y; m_xmf4x4World._23 = xmf3Up.z;
    m_xmf4x4World._31 = xmf3Look.x; m_xmf4x4World._32 = xmf3Look.y; m_xmf4x4World._33 = xmf3Look.z;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
CApacheHellicopter::CApacheHellicopter(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
    LoadGeometryFromFile(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, L"../Assets/Model/Apache.txt");

    m_pRotorFrame = FindFrame(_T("rotor"));
}

CApacheHellicopter::~CApacheHellicopter()
{
}

void CApacheHellicopter::Animate(float fTimeElapsed, CCamera * pCamera)
{
    if (m_pRotorFrame)
    {
        XMMATRIX xmmtxRotate = XMMatrixRotationY(XMConvertToRadians(360.0f * 3.0f) * fTimeElapsed);
        m_pRotorFrame->m_xmf4x4ToParentTransform = Matrix4x4::Multiply(xmmtxRotate, m_pRotorFrame->m_xmf4x4ToParentTransform);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
CGunshipHellicopter::CGunshipHellicopter(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
    LoadGeometryFromFile(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, L"../Assets/Model/Gunship.txt");

    m_pRotorFrame = FindFrame(_T("Rotor"));
    m_pBackRotorFrame = FindFrame(_T("Back_Rotor"));

    m_pHellfileMissileFrame = FindFrame(_T("Hellfire_Missile"));
    if (m_pHellfileMissileFrame) m_pHellfileMissileFrame->m_bHellfireRender = false;

    SetScale(2.0f, 2.0f, 2.0f);
    Rotate(0.0f, 180.0f, 0.0f);
}

CGunshipHellicopter::~CGunshipHellicopter()
{
}

void CGunshipHellicopter::Animate(float fTimeElapsed, CCamera * pCamera)
{
    if (m_pRotorFrame)
    {
        XMMATRIX xmmtxRotate = XMMatrixRotationY(XMConvertToRadians(360.0f * 3.0f) * fTimeElapsed);
        m_pRotorFrame->m_xmf4x4ToParentTransform = Matrix4x4::Multiply(xmmtxRotate, m_pRotorFrame->m_xmf4x4ToParentTransform);
    }

    if (m_pBackRotorFrame)
    {
        XMMATRIX xmmtxRotate = XMMatrixRotationX(XMConvertToRadians(360.0f * 3.0f) * fTimeElapsed);
        m_pBackRotorFrame->m_xmf4x4ToParentTransform = Matrix4x4::Multiply(xmmtxRotate, m_pBackRotorFrame->m_xmf4x4ToParentTransform);
    }
}