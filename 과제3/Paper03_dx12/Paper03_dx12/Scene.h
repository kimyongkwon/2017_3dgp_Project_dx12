#pragma once

#include "Timer.h"
#include "Shader.h"
#include "Player.h"

struct LIGHT
{
    XMFLOAT4				m_xmf4Ambient;
    XMFLOAT4				m_xmf4Diffuse;
    XMFLOAT4				m_xmf4Specular;
    XMFLOAT3				m_xmf3Position;
    float 					m_fFalloff;
    XMFLOAT3				m_xmf3Direction;
    float 					m_fTheta; //cos(m_fTheta)
    XMFLOAT3				m_xmf3Attenuation;
    float					m_fPhi; //cos(m_fPhi)
    bool					m_bEnable;
    int						m_nType;
    float					m_fRange;
    float					padding;
};

struct LIGHTS
{
    enum { PlayerLight = 1, SunLight = 2 };
    LIGHT					m_pLights[MAX_LIGHTS];
    XMFLOAT4				m_xmf4GlobalAmbient;
};

struct MATERIALS
{
    MATERIAL				m_pReflections[MAX_MATERIALS];
};

class CScene
{
public:
    CScene();
    ~CScene();

    bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam); 
    bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
    
    virtual void CreateShaderVariables(ID3D12Device * pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList);
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList * pd3dCommandList);
    virtual void ReleaseShaderVariables();

    void BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
  
    void ReleaseObjects();
    void BuildLightsAndMaterials();
    void UpdateMaterial(CGameObject *pObject);

    ID3D12RootSignature *CreateGraphicsRootSignature(ID3D12Device *pd3dDevice);
    ID3D12RootSignature *GetGraphicsRootSignature();

    bool ProcessInput();    
    void AnimateObjects(float fTimeElapsed, CCamera * pCamera = NULL);    
    void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = NULL);

    void ReleaseUploadBuffers();
    //그래픽 루트 시그너쳐를 생성한다. 
    
    CHeightMapTerrain *GetTerrain() const   { return(m_pTerrain); }

    CPlayer						    *m_pPlayer = NULL;
                                    
protected:                          
    ID3D12RootSignature             *m_pd3dGraphicsRootSignature = NULL;

    CHeightMapTerrain               *m_pTerrain = NULL;
    CSkyBox                         *m_pSkyBox = NULL;

    //배치(Batch) 처리를 하기 위하여 씬을 셰이더들의 리스트로 표현한다. 
    CShader                        **m_ppShaders = NULL; 
    int                              m_nShaders = 0;
    
    //계층구조 관련 오브젝트를 위해 
    CGameObject					**m_ppObjects = NULL;
    int							m_nObjects = 0;

    LIGHTS						    *m_pLights = NULL;
    ID3D12Resource				    *m_pd3dcbLights = NULL;
    LIGHTS						    *m_pcbMappedLights = NULL;

    MATERIALS					    *m_pMaterials = NULL;
    int							    m_nMaterials = 0;

    ID3D12Resource                  *m_pd3dcbMaterials = NULL;
    MATERIAL			            *m_pcbMappedMaterials = NULL;


private:
    int                              m_nDayTime;
public:
    void    SetDayTime(int nDayTime);
    void    CheckOnSunLight(void);

    LIGHT * GetSunLights(void) const    { return &(m_pLights->m_pLights[LIGHTS::SunLight]); }
    LIGHT * GetPlayerLights(void) const { return &(m_pLights->m_pLights[LIGHTS::PlayerLight]); }
};



