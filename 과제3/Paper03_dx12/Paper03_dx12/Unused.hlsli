/////////////////////////////////////////////////////////////////////////////////////////////////
//��ǻ�� ���̴�
struct VS_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

//���� ���̴��� �����Ѵ�. 
VS_OUTPUT VSDiffused(VS_INPUT input)
{
    VS_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.color = input.color;
    return(output);
}

//�ȼ� ���̴��� �����Ѵ�. 
float4 PSDiffused(VS_OUTPUT input) : SV_TARGET
{
    return(input.color);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//�ͷ��� ���̴�

struct VS_TERRAIN_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct VS_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.color = input.color;
    return(output);
}

float4 PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    return(input.color);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//������ �ͷ��� ���̴�

struct VS_LIGHTING_TERRAIN_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
};

struct VS_LIGHTING_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 normalW : NORMAL;
    //	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
    float4 color : COLOR;
#endif
};

VS_LIGHTING_TERRAIN_OUTPUT VSLightingTerrain(VS_LIGHTING_TERRAIN_INPUT input)
{
    VS_LIGHTING_TERRAIN_OUTPUT output;

    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
#ifdef _WITH_VERTEX_LIGHTING
    output.normalW = normalize(output.normalW);
    output.color = Lighting(output.positionW, output.normalW);
#endif
    return(output);
}

float4 PSLightingTerrain(VS_LIGHTING_TERRAIN_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
    return(input.color);
#else
    input.normalW = normalize(input.normalW);
float4 color = Lighting(input.positionW, input.normalW);
return(color);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//�ν��Ͻ̽��̴�

struct VS_INSTANCING_INPUT
{
    float3 position : POSITION;
    float4x4 mtxTransform : WORLDMATRIX;
};

struct VS_INSTANCING_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_INSTANCING_OUTPUT VSInstancing(VS_INSTANCING_INPUT input)
{
    VS_INSTANCING_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), input.mtxTransform), gmtxView), gmtxProjection);
    output.color = float4(0.5f, 0.0f, 0.0f, 0.0f);
    return(output);
}

float4 PSInstancing(VS_INSTANCING_OUTPUT input) : SV_TARGET
{
    return(input.color);
}