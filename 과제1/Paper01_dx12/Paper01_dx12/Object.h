#pragma once
#include "Mesh.h"
#include "Camera.h"

class CShader;

class CRotatingMoveObject;

//게임 객체의 정보를 셰이더에게 넘겨주기 위한 구조체(상수 버퍼)이다. 
struct CB_GAMEOBJECT_INFO
{
    XMFLOAT4X4                      m_xmf4x4World;
    UINT							m_nMaterial;
};

struct MATERIAL
{
    XMFLOAT4						m_xmf4Ambient;
    XMFLOAT4						m_xmf4Diffuse;
    XMFLOAT4						m_xmf4Specular; //(r,g,b,a=power)
    XMFLOAT4						m_xmf4Emissive;
};

class CMaterial
{
public:
    CMaterial();
    virtual ~CMaterial();

private:
    int								m_nReferences = 0;

public:
    void AddRef() { m_nReferences++; }
    void Release() { if (--m_nReferences <= 0) delete this; }

    XMFLOAT4						m_xmf4Albedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

    UINT							m_nReflection = 0;
    CShader							*m_pShader = NULL;

    void SetAlbedo(XMFLOAT4& xmf4Albedo) { m_xmf4Albedo = xmf4Albedo; }
    void SetReflection(UINT nReflection) { m_nReflection = nReflection; }
    void SetShader(CShader *pShader);
};

class CGameObject
{  
private:
    int                             m_nReferences = 0;
public:
    void AddRef()                   { m_nReferences++; }
    void Release()                  { if (--m_nReferences <= 0) delete this; }
protected:
    XMFLOAT4X4                      m_xmf4x4World;
    CShader                        *m_pShader = NULL;
    //게임 객체는 여러 개의 메쉬를 포함하는 경우 게임 객체가 가지는 메쉬들에 대한 포인터와 그 개수이다. 
    CMesh                         **m_ppMeshes = NULL; 
    int                             m_nMeshes = 0;

    BoundingOrientedBox				m_xmBoundingBox;

    ID3D12Resource					*m_pd3dcbGameObject = NULL;
    CB_GAMEOBJECT_INFO				*m_pcbMappedGameObject = NULL;
public:
    CMaterial						*m_pMaterial = NULL;

    D3D12_GPU_DESCRIPTOR_HANDLE		m_d3dCbvGPUDescriptorHandle;

    CGameObject(int nMeshes = 1);
    virtual ~CGameObject();
    void ReleaseUploadBuffers();

    virtual void SetMesh(int nIndex , CMesh *pMesh);
    virtual void SetShader(CShader *pShader);
    void SetMaterial(CMaterial *pMaterial);
    void SetMaterial(UINT nReflection);

    virtual void Animate(float fTimeElapsed);
    virtual void OnPrepareRender();
    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = NULL);
    void Rotate(XMFLOAT3 *pxmf3Axis, float fAngle);

    void SetCbvGPUDescriptorHandle(D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvGPUDescriptorHandle) { m_d3dCbvGPUDescriptorHandle = d3dCbvGPUDescriptorHandle; }

    void SetCbvGPUDescriptorHandle(UINT64 nCbvGPUDescriptorHandlePtr) { m_d3dCbvGPUDescriptorHandle.ptr = nCbvGPUDescriptorHandlePtr; }

    D3D12_GPU_DESCRIPTOR_HANDLE GetCbvGPUDescriptorHandle() { return(m_d3dCbvGPUDescriptorHandle); }

    virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList); 
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList); 

    virtual void SetGraphicsCommandList(ID3D12GraphicsCommandList *pd3dCommandList);

    virtual void ReleaseShaderVariables();

    //게임 객체의 월드 변환 행렬에서 위치 벡터와 방향(x-축, y-축, z-축) 벡터를 반환한다. 
    XMFLOAT3 GetPosition(); 
    XMFLOAT3 GetLook(); 
    XMFLOAT3 GetUp(); 
    XMFLOAT3 GetRight();
    XMFLOAT4X4 GetWorldMtx() { return m_xmf4x4World; };


    //게임 객체의 위치를 설정한다. 
    void SetPosition(float x, float y, float z); 
    void SetPosition(XMFLOAT3 xmf3Position);
    //게임 객체를 로컬 x-축, y-축, z-축 방향으로 이동한다.
    void MoveStrafe(float fDistance = 1.0f); 
    void MoveUp(float fDistance = 1.0f); 
    void MoveForward(float fDistance = 1.0f);
    //게임 객체를 회전(x-축, y-축, z-축)한다. 
    void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);

    virtual XMFLOAT3                        GetDirection(void) {return XMFLOAT3(0.0f, 0.0f, 0.0f);};

    BoundingOrientedBox GetBoundingBox()    { return(m_xmBoundingBox); }
};

class CHeightMapTerrain : public CGameObject 
{
public: 
    CHeightMapTerrain(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature, 
        LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color); 
    virtual ~CHeightMapTerrain();

private: 
    //지형의 높이 맵으로 사용할 이미지이다. 
    CHeightMapImage *m_pHeightMapImage;
    //높이 맵의 가로와 세로 크기이다. 
    int m_nWidth; 
    int m_nLength;
    //지형을 실제로 몇 배 확대할 것인가를 나타내는 스케일 벡터이다. 
    XMFLOAT3 m_xmf3Scale;

public: 
    //지형의 높이를 계산하는 함수이다(월드 좌표계). 높이 맵의 높이에 스케일의 y를 곱한 값이다. 
    float GetHeight(float x, float z) { return(m_pHeightMapImage->GetHeight(x / m_xmf3Scale.x, z / m_xmf3Scale.z) * m_xmf3Scale.y); } 
    //지형의 법선 벡터를 계산하는 함수이다(월드 좌표계). 높이 맵의 법선 벡터를 사용한다. 
    XMFLOAT3 GetNormal(float x, float z) { return(m_pHeightMapImage->GetHeightMapNormal(int(x / m_xmf3Scale.x), int(z / m_xmf3Scale.z))); }

    int GetHeightMapWidth() { return(m_pHeightMapImage->GetHeightMapWidth()); } 
    int GetHeightMapLength() { return(m_pHeightMapImage->GetHeightMapLength()); }

    XMFLOAT3 GetScale() { return(m_xmf3Scale); } 
    //지형의 크기(가로/세로)를 반환한다. 높이 맵의 크기에 스케일을 곱한 값이다. 
    float GetWidth() { return(m_nWidth * m_xmf3Scale.x); } 
    float GetLength() { return(m_nLength * m_xmf3Scale.z); } 

    virtual void SetGraphicsCommandList(ID3D12GraphicsCommandList *pd3dCommandList) {};
    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = NULL);
};

///////////////////////////////////////////////////////////////////////////////////

class CRotatingObject : public CGameObject
{
public:
    CRotatingObject(int nMeshes = 1);
    virtual ~CRotatingObject();
protected:
    XMFLOAT3                        m_xmf3RotationAxis;
    float                           m_fRotationSpeed;
public:
    void                            SetRotationSpeed(float fRotationSpeed) { m_fRotationSpeed = fRotationSpeed; }
    void                            SetRotationAxis(XMFLOAT3 xmf3RotationAxis) { m_xmf3RotationAxis = xmf3RotationAxis; }
    virtual XMFLOAT3                GetDirection(void) { return XMFLOAT3(0.0f, 0.0f, 0.0f); };
    virtual void                    Animate(float fTimeElapsed);
};

///////////////////////////////////////////////////////////////////////////////////

class CRotatingMoveObject : public CRotatingObject
{
public:
    CRotatingMoveObject(int nMeshes = 1);
    virtual ~CRotatingMoveObject();
private:
    XMFLOAT3                        m_xmf3Direction;
    float                           m_fMovingSpeed;

    CHeightMapTerrain             * m_pTerrainData;
public:
    void                            SetDirection(XMFLOAT3 xmf3Direction);
    void                            SetMovingSpeed(float fMovingSpeed);
    void                            SetTerrainData(CHeightMapTerrain * pTerrain);
    virtual XMFLOAT3                GetDirection(void);
    virtual void                    Animate(float fTimeElapsed);
    //게임 객체를 방향벡터에 따라 이동한다.
    void                            Move(void);
    //게임 객체의 높이를 조정한다.
    void                            UpdateByTerrainData(void);
    //게임 객체가 터레인 밖으로 나가는지 확인한다.
    XMFLOAT3                        CheckTerrainOutSide(void);

};