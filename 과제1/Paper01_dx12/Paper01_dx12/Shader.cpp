#include "stdafx.h"
#include "Shader.h"

CShader::~CShader()
{
    if (m_ppd3dPipelineStates)
    {
        for (int i = 0; i < m_nPipelineStates; i++)
            if (m_ppd3dPipelineStates[i]) m_ppd3dPipelineStates[i]->Release();
        delete[] m_ppd3dPipelineStates;
    }
}

//래스터라이저 상태를 설정하기 위한 구조체를 반환한다. 
D3D12_RASTERIZER_DESC CShader::CreateRasterizerState()
{
    D3D12_RASTERIZER_DESC d3dRasterizerDesc;
    ::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
    d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
    //D3D12_FILL_MODE_WIREFRAME은 프리미티브(삼각형)의 내부를 칠하지 않고 변(Edge)만 그린다. 
    //d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
    //d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
    //d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
    d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
    d3dRasterizerDesc.FrontCounterClockwise = FALSE;
    d3dRasterizerDesc.DepthBias = 0;
    d3dRasterizerDesc.DepthBiasClamp = 0.0f;
    d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
    d3dRasterizerDesc.DepthClipEnable = TRUE;
    d3dRasterizerDesc.MultisampleEnable = FALSE;
    d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
    d3dRasterizerDesc.ForcedSampleCount = 0;
    d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
    return(d3dRasterizerDesc);
}

//깊이-스텐실 검사를 위한 상태를 설정하기 위한 구조체를 반환한다. 
D3D12_DEPTH_STENCIL_DESC CShader::CreateDepthStencilState()
{
    D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
    ::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
    d3dDepthStencilDesc.DepthEnable = TRUE;
    d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
    d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
    d3dDepthStencilDesc.StencilEnable = FALSE;
    d3dDepthStencilDesc.StencilReadMask = 0x00;
    d3dDepthStencilDesc.StencilWriteMask = 0x00;
    d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
    d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
    d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
    return(d3dDepthStencilDesc);
}

//블렌딩 상태를 설정하기 위한 구조체를 반환한다. 
D3D12_BLEND_DESC CShader::CreateBlendState()
{
    D3D12_BLEND_DESC d3dBlendDesc;
    ::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
    d3dBlendDesc.AlphaToCoverageEnable = FALSE;
    d3dBlendDesc.IndependentBlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
    d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
    d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
    d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
    d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
    d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
    return(d3dBlendDesc);
}

D3D12_INPUT_LAYOUT_DESC CShader::CreateInputLayout() 
{
    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc; 
    d3dInputLayoutDesc.pInputElementDescs = NULL; 
    d3dInputLayoutDesc.NumElements = 0;
    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob) 
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode; 
    d3dShaderByteCode.BytecodeLength = 0; 
    d3dShaderByteCode.pShaderBytecode = NULL;
    return(d3dShaderByteCode);
}

D3D12_SHADER_BYTECODE CShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob) 
{
    D3D12_SHADER_BYTECODE d3dShaderByteCode; 
    d3dShaderByteCode.BytecodeLength = 0; 
    d3dShaderByteCode.pShaderBytecode = NULL;
    return(d3dShaderByteCode);
}

//셰이더 소스 코드를 컴파일하여 바이트 코드 구조체를 반환한다. 
D3D12_SHADER_BYTECODE CShader::CompileShaderFromFile(WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob **ppd3dShaderBlob)
{
    UINT nCompileFlags = 0;
#if defined(_DEBUG) 
    nCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif 
    ::D3DCompileFromFile(pszFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, pszShaderName, pszShaderProfile, nCompileFlags, 0, ppd3dShaderBlob, NULL);
    D3D12_SHADER_BYTECODE d3dShaderByteCode; 
    d3dShaderByteCode.BytecodeLength = (*ppd3dShaderBlob)->GetBufferSize();
    d3dShaderByteCode.pShaderBytecode = (*ppd3dShaderBlob)->GetBufferPointer();
    return(d3dShaderByteCode);
}

//그래픽스 파이프라인 상태 객체를 생성한다. 
void CShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature) 
{
    ID3DBlob *pd3dVertexShaderBlob = NULL, *pd3dPixelShaderBlob = NULL;
    D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc;
    ::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC)); 
    d3dPipelineStateDesc.pRootSignature = pd3dGraphicsRootSignature; 
    d3dPipelineStateDesc.VS = CreateVertexShader(&pd3dVertexShaderBlob); 
    d3dPipelineStateDesc.PS = CreatePixelShader(&pd3dPixelShaderBlob); 
    d3dPipelineStateDesc.RasterizerState = CreateRasterizerState(); 
    d3dPipelineStateDesc.BlendState = CreateBlendState(); 
    d3dPipelineStateDesc.DepthStencilState = CreateDepthStencilState(); 
    d3dPipelineStateDesc.InputLayout = CreateInputLayout(); 
    d3dPipelineStateDesc.SampleMask = UINT_MAX; 
    d3dPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE; 
    d3dPipelineStateDesc.NumRenderTargets = 1; 
    d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM; 
    d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT; 
    d3dPipelineStateDesc.SampleDesc.Count = 1; 
    d3dPipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE; 
    pd3dDevice->CreateGraphicsPipelineState(&d3dPipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&m_ppd3dPipelineStates[0]);
    if (pd3dVertexShaderBlob) pd3dVertexShaderBlob->Release(); 
    if (pd3dPixelShaderBlob) pd3dPixelShaderBlob->Release();

    if (d3dPipelineStateDesc.InputLayout.pInputElementDescs) delete[] d3dPipelineStateDesc.InputLayout.pInputElementDescs;
}

void CShader::CreateCbvAndSrvDescriptorHeaps(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nConstantBufferViews, int nShaderResourceViews)
{
    D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
    d3dDescriptorHeapDesc.NumDescriptors = nConstantBufferViews + nShaderResourceViews; //CBVs + SRVs 
    d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    d3dDescriptorHeapDesc.NodeMask = 0;
    pd3dDevice->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&m_pd3dCbvSrvDescriptorHeap);

    //힙의 처음 시작 주소값을 알아낸다.(지금 힙은 cbv, srv순으로 되어있다)
    m_d3dCbvCPUDescriptorStartHandle = m_pd3dCbvSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    m_d3dCbvGPUDescriptorStartHandle = m_pd3dCbvSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();

    //힙의 처음 시작 주소값에 cbv의 갯수만큼 주소를 더하면 srv부터의 시작주소가 나온다.
    m_d3dSrvCPUDescriptorStartHandle.ptr = m_d3dCbvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
    m_d3dSrvGPUDescriptorStartHandle.ptr = m_d3dCbvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
}

void CShader::CreateConstantBufferViews(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nConstantBufferViews, ID3D12Resource *pd3dConstantBuffers, UINT nStride)
{
    //버퍼의 처음주소값을 가져온다(힙의 주소값과 헷깔리지마라)
    D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = pd3dConstantBuffers->GetGPUVirtualAddress();

    D3D12_CONSTANT_BUFFER_VIEW_DESC d3dCBVDesc;

    //nStride는 버퍼의 크기다.
    d3dCBVDesc.SizeInBytes = nStride;
    for (int j = 0; j < nConstantBufferViews; j++)
    {
        //버퍼의 주소값을 계산한다.
        d3dCBVDesc.BufferLocation = d3dGpuVirtualAddress + (nStride * j);
        D3D12_CPU_DESCRIPTOR_HANDLE d3dCbvCPUDescriptorHandle;
        d3dCbvCPUDescriptorHandle.ptr = m_d3dCbvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * j);
        //힙에 cbv를 저장한다.
        pd3dDevice->CreateConstantBufferView(&d3dCBVDesc, d3dCbvCPUDescriptorHandle);
    }
}

void CShader::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList) 
{ 
}

void CShader::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList) 
{ 
   
}

void CShader::UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT4X4 *pxmf4x4World) 
{ 
    
}

void CShader::SetGraphicsCommandList(ID3D12GraphicsCommandList *pd3dCommandList)
{
    pd3dCommandList->SetDescriptorHeaps(1, &m_pd3dCbvSrvDescriptorHeap);
}

//셰이더 객체가 포함하는 게임 객체들을 생성한다. 
void CShader::BuildObjects(ID3D12Device * pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList, void * pContext)
{
    CTriangleMesh * pTriangleMesh = new CTriangleMesh(pd3dDevice, pd3dCommandList);
    m_nObjects = 1;
    m_ppObjects = new CGameObject*[m_nObjects];
    m_ppObjects[0] = new CGameObject();
    m_ppObjects[0]->SetMesh(0, pTriangleMesh);
}

void CShader::AnimateObjects(float fTimeElapsed)
{
    for (int j = 0; j < m_nObjects; j++) { m_ppObjects[j]->Animate(fTimeElapsed); }
}

void CShader::ReleaseObjects()
{
    if (m_ppObjects) {
        for (int j = 0; j < m_nObjects; j++)
            if (m_ppObjects[j])
                delete m_ppObjects[j];
        delete[] m_ppObjects;
    }
}

void CShader::ReleaseShaderVariables()
{
   
}

void CShader::OnPrepareRender(ID3D12GraphicsCommandList *pd3dCommandList)
{
    //파이프라인에 그래픽스 상태 객체를 설정한다. 
    pd3dCommandList->SetPipelineState(m_ppd3dPipelineStates[0]);
}

void CShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera) 
{ 
    OnPrepareRender(pd3dCommandList); 
}

////////////////////////////////////////////////

CDiffusedShader::CDiffusedShader() 
{ 
}

CDiffusedShader::~CDiffusedShader() 
{ 
}

D3D12_INPUT_LAYOUT_DESC CDiffusedShader::CreateInputLayout() 
{
    UINT nInputElementDescs = 2; 
    
    D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; 
    pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc; 
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs; 
    d3dInputLayoutDesc.NumElements = nInputElementDescs;
    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CDiffusedShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob) 
{ 
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", ppd3dShaderBlob)); 
}

D3D12_SHADER_BYTECODE CDiffusedShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob) 
{
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", ppd3dShaderBlob));
}

void CDiffusedShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature) 
{
    m_nPipelineStates = 1; 
    m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];

    CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

////////////////////////////////////////////////

CObjectsShader::CObjectsShader() 
{ 
}

CObjectsShader::~CObjectsShader() 
{ 
}

D3D12_INPUT_LAYOUT_DESC CObjectsShader::CreateInputLayout() 
{
    UINT nInputElementDescs = 2; 
    D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
    //pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; 
    //pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[1] = { "NORMAL",0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc; 
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs; 
    d3dInputLayoutDesc.NumElements = nInputElementDescs;
    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CObjectsShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob) 
{ 
    //return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", ppd3dShaderBlob)); 
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSLighting", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CObjectsShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob) 
{ 
    //return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", ppd3dShaderBlob)); 
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSLighting", "ps_5_1", ppd3dShaderBlob));
}

void CObjectsShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature) 
{
    m_nPipelineStates = 1; 
    m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
    CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CObjectsShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, void *pContext) 
{
    CHeightMapTerrain *pTerrain = (CHeightMapTerrain *)pContext; 

    float fTerrainWidth = pTerrain->GetWidth(), fTerrainLength = pTerrain->GetLength();
    float fxPitch = 200.0f; 
    float fzPitch = 200.0f; 
    //직육면체를 지형 표면에 그리고 지형보다 높은 위치에 일정한 간격으로 배치한다. 
    
    int i = 0;
    int xObjects = int(fTerrainWidth / fxPitch),zObjects = int(fTerrainLength / fzPitch); 
    m_nObjects = xObjects * zObjects; 
    m_ppObjects = new CGameObject*[m_nObjects];

    CSphereMeshIlluminated *pSphereMesh = new CSphereMeshIlluminated(pd3dDevice, pd3dCommandList, 10.0f, 20, 20);
    XMFLOAT3 xmf3RotateAxis, xmf3SurfaceNormal; 
    CRotatingMoveObject *pRotatingObject = NULL;

    //Objects만큼 버퍼들을 만들고 버퍼의 포인터를 가져온다.
    CreateShaderVariables(pd3dDevice, pd3dCommandList);

    //버퍼뷰들을 저장할만큼의 Descriptor Heap을 만든다.
    CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, m_nObjects, 0);

    //Descriptor Heap에 버퍼뷰들을 저장한다.
    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);
    CreateConstantBufferViews(pd3dDevice, pd3dCommandList, m_nObjects, m_pd3dcbGameObjects, ncbElementBytes);
    
    D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvSrvGPUDescriptorStartHandle = m_pd3dCbvSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();

    for (int x = 0; x < xObjects; x++) 
    { 
        for (int z = 0; z < zObjects; z++) 
        { 
            pRotatingObject = new CRotatingMoveObject(1);
            pRotatingObject->SetMaterial(i % MAX_MATERIALS);
            pRotatingObject->SetTerrainData(pTerrain);
            pRotatingObject->SetMesh(0, pSphereMesh);
            float xPosition = x * fxPitch;
            float zPosition = z * fzPitch;
            float fHeight = pTerrain->GetHeight(xPosition, zPosition);
            pRotatingObject->SetPosition(xPosition, fHeight + 6.0f, zPosition);
            /*지형의 표면에 위치하는 직육면체는 지형의 기울기에 따라 방향이 다르게 배치한다.
            직육면체가 위치할 지형의 법선 벡터 방향과 직육면체의 y-축이 일치하도록 한다.*/
            //xmf3SurfaceNormal = pTerrain->GetNormal(xPosition, zPosition);
            //xmf3RotateAxis = Vector3::CrossProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal);
            //if (Vector3::IsZero(xmf3RotateAxis))
            //    xmf3RotateAxis = XMFLOAT3(0.0f, 1.0f, 0.0f);
            //float fAngle = acos(Vector3::DotProduct(XMFLOAT3(0.0f, 1.0f, 0.0f), xmf3SurfaceNormal));
            //pRotatingObject->Rotate(&xmf3RotateAxis, XMConvertToDegrees(fAngle));
            pRotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
            pRotatingObject->SetRotationSpeed(36.0f * (i % 10) + 36.0f);
            pRotatingObject->SetDirection(XMFLOAT3((double)(rand() % 100 - 50) / 100, 0.0f, (double)(rand() % 100 - 50) / 100));
            pRotatingObject->SetMovingSpeed(0.1f);
            //각자의 오브젝트들을 자신의 오브젝트 정보를 담고있는 view들의 주소를 디스크립터핸들로 갖고있는다.
            pRotatingObject->SetCbvGPUDescriptorHandle(d3dCbvSrvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * i));
            pRotatingObject->GetBoundingBox() = BoundingOrientedBox(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(10.0f, 10.0f, 10.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
            m_ppObjects[i++] = pRotatingObject;
        } 
    }
    
}

void CObjectsShader::ReleaseObjects() 
{ 
    if (m_ppObjects) 
    { 
        for (int j = 0; j < m_nObjects; j++) 
        { 
            if (m_ppObjects[j]) delete m_ppObjects[j]; 
        } 
        delete[] m_ppObjects; 
    } 
}

void CObjectsShader::AnimateObjects(float fTimeElapsed) 
{ 
    for (int j = 0; j < m_nObjects; j++) 
    {
        m_ppObjects[j]->Animate(fTimeElapsed); 
    } 
}

//이 함수는 루트파라미터가 RootConstant였다면 호출되지 않았을것이다.
//따로 버퍼를 만들고, 디스크립터를 만들필요가 없기때문에.
void CObjectsShader::CreateShaderVariables(ID3D12Device * pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList)
{
    //1. 리소스를 만든다.
    UINT ncbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    m_pd3dcbGameObjects = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbGameObjectBytes * m_nObjects, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

    //2. 리소스 포인터를 얻는다.
    m_pd3dcbGameObjects->Map(0, NULL, (void **)&m_pcbMappedGameObjects);

    ////3. 디스크립터 힙을 만든다.
    //D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
    //d3dDescriptorHeapDesc.NumDescriptors = m_nObjects;
    //d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    //d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    //d3dDescriptorHeapDesc.NodeMask = 0;
    //pd3dDevice->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&m_pd3dCbvSrvDescriptorHeap);

    ////4. cpu, gpu 디스크립터 핸들을 얻는다.
    //D3D12_CPU_DESCRIPTOR_HANDLE d3dCbvSrvCPUDescriptorStartHandle = m_pd3dCbvSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    //D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvSrvGPUDescriptorStartHandle = m_pd3dCbvSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();

    ////5. 리소스 GPU 주소값을 얻는다.
    //D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbGameObjects->GetGPUVirtualAddress();
    //D3D12_CONSTANT_BUFFER_VIEW_DESC d3dcbvDesc;
    //d3dcbvDesc.SizeInBytes = ncbGameObjectBytes;

    ////다음은 오브젝트마다 view를 따로 갖지고 있다.
    ////하지만 LabProject30처럼 view를 하나만 있어도 되지않을까? 
    ////정답 : 안됌 
    ////왜? 함수가 그렇게 제공되고있지않음
    ////그렇게하려면 runtime에 device에 접근해야한다. device는 주로 어떤걸 만드는 함수이기때문에 runtime에 device에 접근해서 view를 따로 만드는것은 좋지않다.
    //
    ////6. 오브젝트수에 맞게 디스크립터 힙에 View를 하나씩 생성해준다. 
    //for (int j = 0; j < m_nObjects; j++)
    //{
    //    d3dcbvDesc.BufferLocation = d3dGpuVirtualAddress + (ncbGameObjectBytes * j);
    //    D3D12_CPU_DESCRIPTOR_HANDLE d3dCbvCPUDescriptorHandle;
    //    d3dCbvCPUDescriptorHandle.ptr = d3dCbvSrvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * j);
    //    pd3dDevice->CreateConstantBufferView(&d3dcbvDesc, d3dCbvCPUDescriptorHandle);
    //}
}

void CObjectsShader::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    UINT ncbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    for (int j = 0; j < m_nObjects; j++)
    {
        CB_GAMEOBJECT_INFO *pbMappedcbGameObject = (CB_GAMEOBJECT_INFO *)(m_pcbMappedGameObjects + (j * ncbGameObjectBytes));
        XMStoreFloat4x4(&pbMappedcbGameObject->m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_ppObjects[j]->GetWorldMtx())));
        pbMappedcbGameObject->m_nMaterial = m_ppObjects[j]->m_pMaterial->m_nReflection;
    }
}

void CObjectsShader::ReleaseUploadBuffers()
{ 
    if (m_ppObjects) 
    { 
        for (int j = 0; j < m_nObjects; j++) 
            m_ppObjects[j]->ReleaseUploadBuffers(); 
    } 
}

void CObjectsShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera) 
{
    CShader::Render(pd3dCommandList, pCamera);

    //Objects들에 관한 정보(버퍼)는 쉐이더가 다 가지고 있다.
    //다음함수에서 버퍼들을 메모리 카피한다.
    UpdateShaderVariables(pd3dCommandList);

    //Objects를 그릴때마다 호출된다.
    CShader::SetGraphicsCommandList(pd3dCommandList);

    for (int j = 0; j < m_nObjects; j++) 
    { 
        if (m_ppObjects[j]) 
            m_ppObjects[j]->Render(pd3dCommandList, pCamera); 
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CTerrainShader::CTerrainShader() 
{ 
}

CTerrainShader::~CTerrainShader() 
{ 
}

D3D12_INPUT_LAYOUT_DESC CTerrainShader::CreateInputLayout() 
{
    UINT nInputElementDescs = 2; 
    D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
    //pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; 
    //pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[1] = { "NORMAL",0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc; 
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs; 
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CTerrainShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob) 
{ 
    //return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSLighting", "ps_5_1", ppd3dShaderBlob));

    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSLightingTerrain", "vs_5_1", ppd3dShaderBlob)); 
}

D3D12_SHADER_BYTECODE CTerrainShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob) 
{ 
    //return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSLighting", "ps_5_1", ppd3dShaderBlob)); 

    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSLightingTerrain", "ps_5_1", ppd3dShaderBlob));
}

void CTerrainShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature) 
{
    m_nPipelineStates = 1; 
    m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];

    CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}


void CTerrainShader::Render(ID3D12GraphicsCommandList * pd3dCommandList, CCamera * pCamera)
{
    CShader::Render(pd3dCommandList, pCamera);

    //D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbTerrain->GetGPUVirtualAddress();
    //pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dGpuVirtualAddress);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CPlayerShader::CPlayerShader()
{
}

CPlayerShader::~CPlayerShader()
{
}

D3D12_INPUT_LAYOUT_DESC CPlayerShader::CreateInputLayout()
{
    UINT nInputElementDescs = 2;
    D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

    pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
    pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

    D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
    d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
    d3dInputLayoutDesc.NumElements = nInputElementDescs;

    return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CPlayerShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSPlayer", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CPlayerShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
    return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSPlayer", "ps_5_1", ppd3dShaderBlob));
}

void CPlayerShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
    m_nPipelineStates = 1;
    m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];

    CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CPlayerShader::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
    UINT ncbElementBytes = ((sizeof(CB_PLAYER_INFO) + 255) & ~255); //256의 배수
    m_pd3dcbPlayer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

    m_pd3dcbPlayer->Map(0, NULL, (void **)&m_pcbMappedPlayer);
}

void CPlayerShader::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
}

void CPlayerShader::UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT4X4 *pxmf4x4World)
{
    XMFLOAT4X4 xmf4x4World;
    XMStoreFloat4x4(&xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(pxmf4x4World)));
    ::memcpy(&m_pcbMappedPlayer->m_xmf4x4World, &xmf4x4World, sizeof(XMFLOAT4X4));
}

void CPlayerShader::ReleaseShaderVariables()
{
    if (m_pd3dcbPlayer)
    {
        m_pd3dcbPlayer->Unmap(0, NULL);
        m_pd3dcbPlayer->Release();
    }
}

void CPlayerShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
    CShader::Render(pd3dCommandList, pCamera);

    D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbPlayer->GetGPUVirtualAddress();
    pd3dCommandList->SetGraphicsRootConstantBufferView(3, d3dGpuVirtualAddress);
}
