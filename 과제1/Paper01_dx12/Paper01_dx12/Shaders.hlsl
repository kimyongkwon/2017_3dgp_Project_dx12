//////////////////////////////////////////////////////////////////////////////////////////

#include "Header.hlsl"
#include "Light.hlsl"

/////////////////////////////////////////////////////////////////////////////////////////////////
//디퓨즈 쉐이더

struct VS_INPUT 
{ 
    float3 position : POSITION; 
    float4 color : COLOR; 
};

struct VS_OUTPUT 
{ 
    float4 position : SV_POSITION; 
    float4 color : COLOR; 
};

//정점 셰이더를 정의한다. 
VS_OUTPUT VSDiffused(VS_INPUT input) 
{ 
    VS_OUTPUT output; 
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection); 
    output.color = input.color;
    return(output);
}

//픽셀 셰이더를 정의한다. 
float4 PSDiffused(VS_OUTPUT input) : SV_TARGET 
{ 
    return(input.color); 
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//플레이어 쉐이더

VS_OUTPUT VSPlayer(VS_INPUT input)
{
    VS_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxPlayerWorld), gmtxView), gmtxProjection);
    output.color = input.color;

    return(output);
}

float4 PSPlayer(VS_OUTPUT input) : SV_TARGET
{
    return(input.color);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//터레인 쉐이더

struct VS_TERRAIN_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct VS_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.color = input.color;
    return(output);
}

float4 PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    return(input.color);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//라이팅 터레인 쉐이더

#define _WITH_VERTEX_LIGHTING

struct VS_LIGHTING_TERRAIN_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
};

struct VS_LIGHTING_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 normalW : NORMAL;
    //	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
    float4 color : COLOR;
#endif
};

VS_LIGHTING_TERRAIN_OUTPUT VSLightingTerrain(VS_LIGHTING_TERRAIN_INPUT input)
{
    VS_LIGHTING_TERRAIN_OUTPUT output;

    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
#ifdef _WITH_VERTEX_LIGHTING
    output.normalW = normalize(output.normalW);
    output.color = Lighting(output.positionW, output.normalW);
#endif
    return(output);
}

float4 PSLightingTerrain(VS_LIGHTING_TERRAIN_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
    return(input.color);
#else
    input.normalW = normalize(input.normalW);
float4 color = Lighting(input.positionW, input.normalW);
return(color);
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//라이팅 오브젝트 쉐이더

struct VS_LIGHTING_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
};

struct VS_LIGHTING_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 normalW : NORMAL;
    //	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
    float4 color : COLOR;
#endif
};

VS_LIGHTING_OUTPUT VSLighting(VS_LIGHTING_INPUT input)
{
    VS_LIGHTING_OUTPUT output;

    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
#ifdef _WITH_VERTEX_LIGHTING
    output.normalW = normalize(output.normalW);
    output.color = Lighting(output.positionW, output.normalW);
#endif
    return(output);
}

float4 PSLighting(VS_LIGHTING_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
    return(input.color);
#else
    input.normalW = normalize(input.normalW);
float4 color = Lighting(input.positionW, input.normalW);
return(color);
#endif
}





