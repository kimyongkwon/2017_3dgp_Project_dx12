#include "stdafx.h"
#include "Scene.h"
#include "Camera.h"

CScene::CScene()
{
    m_pd3dGraphicsRootSignature = NULL;
    m_nDayTime = 0;

    m_ppShaders = NULL;
}

CScene::~CScene()
{
}

void CScene::BuildLightsAndMaterials()
{
    m_pLights = new LIGHTS;
    ::ZeroMemory(m_pLights, sizeof(LIGHTS));

    m_pLights->m_xmf4GlobalAmbient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);

    m_pLights->m_pLights[0].m_bEnable = true;
    m_pLights->m_pLights[0].m_nType = POINT_LIGHT;
    m_pLights->m_pLights[0].m_fRange = 100.0f;
    m_pLights->m_pLights[0].m_xmf4Ambient = XMFLOAT4(0.1f, 0.0f, 0.0f, 1.0f);
    m_pLights->m_pLights[0].m_xmf4Diffuse = XMFLOAT4(0.8f, 0.0f, 0.0f, 1.0f);
    m_pLights->m_pLights[0].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
    m_pLights->m_pLights[0].m_xmf3Position = XMFLOAT3(130.0f, 30.0f, 30.0f);
    m_pLights->m_pLights[0].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_pLights->m_pLights[0].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.001f, 0.0001f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_bEnable = true;
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_nType = SPOT_LIGHT;
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_fRange = 50.0f;
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf4Diffuse = XMFLOAT4(0.4f, 0.4f, 0.4f, 1.0f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf4Specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf3Position = XMFLOAT3(-50.0f, 20.0f, -5.0f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 1.0f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_fFalloff = 8.0f;
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_fPhi = (float)cos(XMConvertToRadians(40.0f));
    m_pLights->m_pLights[LIGHTS::PlayerLight].m_fTheta = (float)cos(XMConvertToRadians(20.0f));
    m_pLights->m_pLights[LIGHTS::SunLight].m_bEnable = true;
    m_pLights->m_pLights[LIGHTS::SunLight].m_nType = DIRECTIONAL_LIGHT;
    m_pLights->m_pLights[LIGHTS::SunLight].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_pLights->m_pLights[LIGHTS::SunLight].m_xmf4Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
    m_pLights->m_pLights[LIGHTS::SunLight].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
    m_pLights->m_pLights[LIGHTS::SunLight].m_xmf3Direction = XMFLOAT3(0.0f, -1.0f, 0.0f);
    m_pLights->m_pLights[3].m_bEnable = true;
    m_pLights->m_pLights[3].m_nType = SPOT_LIGHT;
    m_pLights->m_pLights[3].m_fRange = 60.0f;
    m_pLights->m_pLights[3].m_xmf4Ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_pLights->m_pLights[3].m_xmf4Diffuse = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
    m_pLights->m_pLights[3].m_xmf4Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
    m_pLights->m_pLights[3].m_xmf3Position = XMFLOAT3(-150.0f, 30.0f, 30.0f);
    m_pLights->m_pLights[3].m_xmf3Direction = XMFLOAT3(0.0f, 1.0f, 1.0f);
    m_pLights->m_pLights[3].m_xmf3Attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_pLights->m_pLights[3].m_fFalloff = 8.0f;
    m_pLights->m_pLights[3].m_fPhi = (float)cos(XMConvertToRadians(90.0f));
    m_pLights->m_pLights[3].m_fTheta = (float)cos(XMConvertToRadians(30.0f));

    m_pMaterials = new MATERIALS;
    ::ZeroMemory(m_pMaterials, sizeof(MATERIALS)); 

    m_pMaterials->m_pReflections[0] = { XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[1] = { XMFLOAT4(0.0f, 0.2f, 0.0f, 1.0f), XMFLOAT4(0.6f, 0.9f, 0.2f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[2] = { XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 15.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[3] = { XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 20.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[4] = { XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 25.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[5] = { XMFLOAT4(0.0f, 0.5f, 0.5f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 30.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[6] = { XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 35.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
    m_pMaterials->m_pReflections[7] = { XMFLOAT4(1.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 40.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
}

void CScene::ReleaseShaderVariables()
{
    if (m_pd3dcbLights)
    {
        m_pd3dcbLights->Unmap(0, NULL);
        m_pd3dcbLights->Release();
    }
    if (m_pd3dcbMaterials)
    {
        m_pd3dcbMaterials->Unmap(0, NULL);
        m_pd3dcbMaterials->Release();
    }
}

void CScene::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
    m_pd3dGraphicsRootSignature = CreateGraphicsRootSignature(pd3dDevice);

    //조명과 재질을 초기화한다.
    BuildLightsAndMaterials();

    //조명과 재질 정보를 이용해서 리소스를 만든다.
    CreateShaderVariables(pd3dDevice, pd3dCommandList);

    //지형을 확대할 스케일 벡터이다. x-축과 z-축은 8배, y-축은 2배 확대한다. 
    XMFLOAT3 xmf3Scale(8.0f, 2.0f, 8.0f); 
    XMFLOAT4 xmf4Color(0.0f, 0.5f, 0.0f, 0.0f);
    //지형을 높이 맵 이미지 파일(HeightMap.raw)을 사용하여 생성한다. 높이 맵의 크기는 가로x세로(257x257)이다. 
#ifdef _WITH_TERRAIN_PARTITION 
    /*하나의 격자 메쉬의 크기는 가로x세로(17x17)이다. 지형 전체는 가로 방향으로 16개,
    세로 방향으로 16의 격자 메 쉬를 가진다. 지형을 구성하는 격자 메쉬의 개수는 총 256(16x16)개가 된다.*/
    m_pTerrain = new CHeightMapTerrain(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature,
        _T("../Assets/Image/Terrain/HeightMap.raw"), 257, 257, 17, 17, xmf3Scale, xmf4Color);
#else //지형을 하나의 격자 메쉬(257x257)로 생성한다.  
    m_pTerrain = new CHeightMapTerrain(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature,
        _T("../Assets/Image/Terrain/HeightMap.raw"), 257, 257, 257, 257, xmf3Scale, xmf4Color);
#endif

    m_pSkyBox = new CSkyBox(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature);

    m_nShaders = 4;
    m_ppShaders = new CShader*[m_nShaders];

    CSubData * SubDataForBuild = new CSubData();
    SubDataForBuild->SetTerrainData(m_pTerrain);
    m_ppShaders[0] = new CObjectsShader;
    m_ppShaders[0]->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature);
    m_ppShaders[0]->BuildObjects(pd3dDevice, pd3dCommandList, SubDataForBuild);

    SubDataForBuild->SetBillboardData(0, XMFLOAT3(80.0f, 80.0f, 80.0f), 2000);
    m_ppShaders[1] = new CInstancingShader;
    m_ppShaders[1]->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature);
    m_ppShaders[1]->BuildObjects(pd3dDevice, pd3dCommandList, SubDataForBuild);

    SubDataForBuild->SetBillboardData(1, XMFLOAT3(20.0f, 20.0f, 20.0f), 200);
    m_ppShaders[2] = new CInstancingShader;
    m_ppShaders[2]->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature);
    m_ppShaders[2]->BuildObjects(pd3dDevice, pd3dCommandList, SubDataForBuild);

    SubDataForBuild->SetBillboardData(2, XMFLOAT3(20.0f, 20.0f, 20.0f), 200);
    m_ppShaders[3] = new CInstancingShader;
    m_ppShaders[3]->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature);
    m_ppShaders[3]->BuildObjects(pd3dDevice, pd3dCommandList, SubDataForBuild);

    delete SubDataForBuild;
}

void CScene::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
    //RootDescriptor는 파라미터가 힙을 대신하기떄문에 힙을 따로 만들어줄 필요가 없다.
    HRESULT hresult;

    UINT ncbElementBytes = ((sizeof(LIGHTS) + 255) & ~255); //256의 배수
    m_pd3dcbLights = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
    hresult = m_pd3dcbLights->Map(0, NULL, (void **)&m_pcbMappedLights);
    assert("Map error" && hresult == S_OK);

    UINT ncbMaterialBytes = ((sizeof(MATERIALS) + 255) & ~255); //256의 배수
    m_pd3dcbMaterials = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbMaterialBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
    hresult = m_pd3dcbMaterials->Map(0, NULL, (void **)&m_pcbMappedMaterials);
    assert("Map error" && hresult == S_OK);
}

void CScene::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    ::memcpy(m_pcbMappedLights, m_pLights, sizeof(LIGHTS));
    ::memcpy(m_pcbMappedMaterials, m_pMaterials, sizeof(MATERIALS));

    D3D12_GPU_VIRTUAL_ADDRESS d3dcbLightsGpuVirtualAddress = m_pd3dcbLights->GetGPUVirtualAddress();
    pd3dCommandList->SetGraphicsRootConstantBufferView(4, d3dcbLightsGpuVirtualAddress);

    D3D12_GPU_VIRTUAL_ADDRESS d3dcbMaterialsGpuVirtualAddress = m_pd3dcbMaterials->GetGPUVirtualAddress();
    pd3dCommandList->SetGraphicsRootConstantBufferView(5, d3dcbMaterialsGpuVirtualAddress);
}

void CScene::ReleaseObjects() 
{
    if (m_pd3dGraphicsRootSignature) m_pd3dGraphicsRootSignature->Release();

    for (int i = 0; i < m_nShaders; i++) 
    { 
        m_ppShaders[i]->ReleaseShaderVariables(); 
        m_ppShaders[i]->ReleaseObjects(); 
    } 

    ReleaseShaderVariables();

    for (int i = 0; i < m_nShaders; i++)
    {
        if (m_ppShaders[i])
            delete m_ppShaders[i];
    }
    if (m_pLights) delete m_pLights;
    if (m_pMaterials) delete m_pMaterials;
    if (m_pTerrain) delete m_pTerrain;
    if (m_pSkyBox) delete m_pSkyBox;
}

void CScene::ReleaseUploadBuffers() 
{ 
    for (int i = 0; i < m_nShaders; i++) 
        m_ppShaders[i]->ReleaseUploadBuffers(); 

    if (m_pTerrain) m_pTerrain->ReleaseUploadBuffers();
}

void CScene::CheckOnSunLight(void)
{
    m_nDayTime++;

    static const int MaxDayTime = 3000;
    static const int MaxNightTime = 6000;

    if (m_nDayTime > 0 && m_nDayTime <= MaxDayTime) m_pLights->m_pLights[LIGHTS::SunLight].m_bEnable = true;
    else if (m_nDayTime > MaxDayTime && m_nDayTime <= MaxNightTime) m_pLights->m_pLights[LIGHTS::SunLight].m_bEnable = false;
    else if (m_nDayTime >= MaxNightTime) m_nDayTime = 0;
}

void CScene::SetDayTime(int nDayTime)
{
    m_nDayTime = nDayTime;
}

ID3D12RootSignature *CScene::GetGraphicsRootSignature()
{
    return(m_pd3dGraphicsRootSignature);
}

ID3D12RootSignature *CScene::CreateGraphicsRootSignature(ID3D12Device *pd3dDevice) 
{
    ID3D12RootSignature *pd3dGraphicsRootSignature = NULL;

    D3D12_DESCRIPTOR_RANGE pd3dDescriptorRanges[7];
    pd3dDescriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[0].NumDescriptors = 1;
    pd3dDescriptorRanges[0].BaseShaderRegister = 0; //Game Objects
    pd3dDescriptorRanges[0].RegisterSpace = 0;
    pd3dDescriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[1].NumDescriptors = 1;
    pd3dDescriptorRanges[1].BaseShaderRegister = 0; //t0 : gtxtSkyBoxTexture
    pd3dDescriptorRanges[1].RegisterSpace = 0;
    pd3dDescriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[2].NumDescriptors = 1;
    pd3dDescriptorRanges[2].BaseShaderRegister = 1; //t1 : gtxtTerrainBaseTexture
    pd3dDescriptorRanges[2].RegisterSpace = 0;
    pd3dDescriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[3].NumDescriptors = 1;
    pd3dDescriptorRanges[3].BaseShaderRegister = 2; //t2 : gtxtTerrainDetailTexture
    pd3dDescriptorRanges[3].RegisterSpace = 0;
    pd3dDescriptorRanges[3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[4].NumDescriptors = 1;
    pd3dDescriptorRanges[4].BaseShaderRegister = 3; //t3 : gtxtTreeTexture
    pd3dDescriptorRanges[4].RegisterSpace = 0;
    pd3dDescriptorRanges[4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[5].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[5].NumDescriptors = 1;
    pd3dDescriptorRanges[5].BaseShaderRegister = 4; //t4 : gtxtFlowerTexture
    pd3dDescriptorRanges[5].RegisterSpace = 0;
    pd3dDescriptorRanges[5].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    pd3dDescriptorRanges[6].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV; //root descrpitor와 다르게 뷰를 직접 만들어줘야한다.
    pd3dDescriptorRanges[6].NumDescriptors = 1;
    pd3dDescriptorRanges[6].BaseShaderRegister = 5; //t5 : gtxtGrassTexture
    pd3dDescriptorRanges[6].RegisterSpace = 0;
    pd3dDescriptorRanges[6].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

    D3D12_ROOT_PARAMETER pd3dRootParameters[11]; 
    //Game Objects
    pd3dRootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[0].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[0].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[0];   
    pd3dRootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //Camera
    pd3dRootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
    pd3dRootParameters[1].Descriptor.ShaderRegister = 1;
    pd3dRootParameters[1].Descriptor.RegisterSpace = 0;
    pd3dRootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //SkyBox
    pd3dRootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[2].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[2].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[1];
    pd3dRootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //Player
    pd3dRootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
    pd3dRootParameters[3].Descriptor.ShaderRegister = 3;
    pd3dRootParameters[3].Descriptor.RegisterSpace = 0;
    pd3dRootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //Lights
    pd3dRootParameters[4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
    pd3dRootParameters[4].Descriptor.ShaderRegister = 4; 
    pd3dRootParameters[4].Descriptor.RegisterSpace = 0;
    pd3dRootParameters[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //Materials
    pd3dRootParameters[5].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
    pd3dRootParameters[5].Descriptor.ShaderRegister = 5; 
    pd3dRootParameters[5].Descriptor.RegisterSpace = 0;
    pd3dRootParameters[5].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //TerrainBaseTexture
    pd3dRootParameters[6].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[6].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[6].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[2];
    pd3dRootParameters[6].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //TerrainDetailTexture
    pd3dRootParameters[7].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[7].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[7].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[3];
    pd3dRootParameters[7].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //BillboardTexture(Tree)
    pd3dRootParameters[8].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[8].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[8].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[4];
    pd3dRootParameters[8].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //BillboardTexture(Flower)
    pd3dRootParameters[9].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[9].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[9].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[5];
    pd3dRootParameters[9].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //BillboardTexture(Grass)
    pd3dRootParameters[10].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    pd3dRootParameters[10].DescriptorTable.NumDescriptorRanges = 1;
    pd3dRootParameters[10].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[6];
    pd3dRootParameters[10].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

    //Static Samlper
    D3D12_STATIC_SAMPLER_DESC pd3dSamplerDescs[2];
    pd3dSamplerDescs[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
    pd3dSamplerDescs[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    pd3dSamplerDescs[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    pd3dSamplerDescs[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    pd3dSamplerDescs[0].MipLODBias = 0;
    pd3dSamplerDescs[0].MaxAnisotropy = 1;
    pd3dSamplerDescs[0].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
    pd3dSamplerDescs[0].MinLOD = 0;
    pd3dSamplerDescs[0].MaxLOD = D3D12_FLOAT32_MAX;
    pd3dSamplerDescs[0].ShaderRegister = 0;
    pd3dSamplerDescs[0].RegisterSpace = 0;
    pd3dSamplerDescs[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

    pd3dSamplerDescs[1].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
    pd3dSamplerDescs[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    pd3dSamplerDescs[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    pd3dSamplerDescs[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    pd3dSamplerDescs[1].MipLODBias = 0;
    pd3dSamplerDescs[1].MaxAnisotropy = 1;
    pd3dSamplerDescs[1].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
    pd3dSamplerDescs[1].MinLOD = 0;
    pd3dSamplerDescs[1].MaxLOD = D3D12_FLOAT32_MAX;
    pd3dSamplerDescs[1].ShaderRegister = 1;
    pd3dSamplerDescs[1].RegisterSpace = 0;
    pd3dSamplerDescs[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

    //★★★주의★★★
    D3D12_ROOT_SIGNATURE_FLAGS d3dRootSignatureFlags =
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |     //IA 단계를 허용
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |           //헐쉐이더가 접근할 수 없음
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |         //도메인쉐이더가 접근할 수 없음
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;        //기하쉐이더가 접근할 수 없음
    D3D12_ROOT_SIGNATURE_DESC d3dRootSignatureDesc; 
    ::ZeroMemory(&d3dRootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC)); 
    d3dRootSignatureDesc.NumParameters = _countof(pd3dRootParameters); 
    d3dRootSignatureDesc.pParameters = pd3dRootParameters; 
    d3dRootSignatureDesc.NumStaticSamplers = _countof(pd3dSamplerDescs);
    d3dRootSignatureDesc.pStaticSamplers = pd3dSamplerDescs;
    d3dRootSignatureDesc.Flags = d3dRootSignatureFlags;

    ID3DBlob *pd3dSignatureBlob = NULL;
    ID3DBlob *pd3dErrorBlob = NULL;
    
    HRESULT hresult = D3D12SerializeRootSignature(&d3dRootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &pd3dSignatureBlob, &pd3dErrorBlob);
    assert("SerializeRootSignature error" && hresult == S_OK);
    hresult = pd3dDevice->CreateRootSignature(0, pd3dSignatureBlob->GetBufferPointer(), pd3dSignatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void **)&pd3dGraphicsRootSignature);
    assert("CreateRootSignature error" && hresult == S_OK);
    if (pd3dSignatureBlob) pd3dSignatureBlob->Release();
    if (pd3dErrorBlob) pd3dErrorBlob->Release();

    return(pd3dGraphicsRootSignature);
}

bool CScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
    return(false);
}

bool CScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
    return(false);
}

bool CScene::ProcessInput()
{
    return(false);
}

void CScene::AnimateObjects(float fTimeElapsed, CCamera * pCamera)
{ 
    CheckOnSunLight();

    for (int i = 0; i < m_nShaders; i++) 
    { 
        m_ppShaders[i]->AnimateObjects(fTimeElapsed, pCamera); 
    }

    if (m_pLights)
    {
        m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf3Position = m_pPlayer->GetPosition();
        m_pLights->m_pLights[LIGHTS::PlayerLight].m_xmf3Direction = m_pPlayer->GetLookVector();
    }
}

void CScene::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera) 
{
    pCamera->SetViewportsAndScissorRects(pd3dCommandList); 
    pd3dCommandList->SetGraphicsRootSignature(m_pd3dGraphicsRootSignature);
    if (pCamera) pCamera->UpdateShaderVariables(pd3dCommandList);

    //조명과 재질 정보 업데이트
    UpdateShaderVariables(pd3dCommandList);

    if (m_pSkyBox) m_pSkyBox->Render(pd3dCommandList, pCamera);
    if (m_pTerrain) m_pTerrain->Render(pd3dCommandList, pCamera);

    //씬을 렌더링하는 것은 씬을 구성하는 게임 객체(셰이더를 포함하는 객체)들을 렌더링하는 것이다. 
    for (int i = 0; i < m_nShaders; i++) 
    { 
        m_ppShaders[i]->Render(pd3dCommandList, pCamera); 
    }
}

