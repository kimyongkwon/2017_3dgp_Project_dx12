
#include "Header.hlsl"
#include "Light.hlsl"

/////////////////////////////////////////////////////////////////////////////////////////////////
//플레이어 쉐이더

struct VS_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_OUTPUT VSPlayer(VS_INPUT input)
{
    VS_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxPlayerWorld), gmtxView), gmtxProjection);
    output.color = input.color;

    return(output);
}

float4 PSPlayer(VS_OUTPUT input) : SV_TARGET
{
    return(input.color);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//라이팅 디테일 텍스쳐 터레인 쉐이더

struct VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
};

struct VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 normalW : NORMAL;
    //	nointerpolation float3 normalW : NORMAL;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;

#ifdef _WITH_VERTEX_LIGHTING
    float4 color : COLOR;
#endif
};

VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_OUTPUT VSLightingDetailTexturedTerrain(VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_INPUT input)
{
    VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_OUTPUT output = (VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_OUTPUT)0;

    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
#ifdef _WITH_VERTEX_LIGHTING
    output.normalW = normalize(output.normalW);
    output.color = Lighting(output.positionW, output.normalW);
#endif
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;

    return(output);
}

float4 PSLightingDetailTexturedTerrain(VS_LIGHTING_DETAIL_TEXTURED_TERRAIN_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
    float4 cIllumination = input.color;
#else
    input.normalW = normalize(input.normalW);
    float4 cIllumination = Lighting(input.positionW, input.normalW);
#endif

    float4 cBaseTexColor = gtxtTerrainBaseTexture.Sample(gWrapSamplerState, input.uv0);
    float4 cDetailTexColor = gtxtTerrainDetailTexture.Sample(gWrapSamplerState, input.uv1);
    float4 color = saturate((cBaseTexColor * 0.5f) + (cDetailTexColor * 0.5f));

    return(color * cIllumination);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//라이팅 오브젝트 쉐이더

struct VS_LIGHTING_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
};

struct VS_LIGHTING_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 normalW : NORMAL;
    //	nointerpolation float3 normalW : NORMAL;
#ifdef _WITH_VERTEX_LIGHTING
    float4 color : COLOR;
#endif
};

VS_LIGHTING_OUTPUT VSLighting(VS_LIGHTING_INPUT input)
{
    VS_LIGHTING_OUTPUT output;

    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
#ifdef _WITH_VERTEX_LIGHTING
    output.normalW = normalize(output.normalW);
    output.color = Lighting(output.positionW, output.normalW);
#endif
    return(output);
}

float4 PSLighting(VS_LIGHTING_OUTPUT input) : SV_TARGET
{
#ifdef _WITH_VERTEX_LIGHTING
    return(input.color);
#else
    input.normalW = normalize(input.normalW);
float4 color = Lighting(input.positionW, input.normalW);
return(color);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////텍스쳐 쉐이더

struct VS_TEXTURED_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VSTextured(VS_TEXTURED_INPUT input)
{
    VS_TEXTURED_OUTPUT output;

//#ifdef _WITH_CONSTANT_BUFFER_SYNTAX
    //output.position = mul(mul(mul(float4(input.position, 1.0f), gcbGameObjectInfo.mtxWorld), gcbCameraInfo.mtxView), gcbCameraInfo.mtxProjection);
//#else
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
//#endif
    output.uv = input.uv;

    return(output);
}

//float4 PSTextured(VS_TEXTURED_OUTPUT input, uint nPrimitiveID : SV_PrimitiveID) : SV_TARGET
//{
//    /*
//    float4 cColor;
//    if (nPrimitiveID < 2)
//    cColor = gtxtTextures[0].Sample(gWrapSamplerState, input.uv);
//    else if (nPrimitiveID < 4)
//    cColor = gtxtTextures[1].Sample(gWrapSamplerState, input.uv);
//    else if (nPrimitiveID < 6)
//    cColor = gtxtTextures[2].Sample(gWrapSamplerState, input.uv);
//    else if (nPrimitiveID < 8)
//    cColor = gtxtTextures[3].Sample(gWrapSamplerState, input.uv);
//    else if (nPrimitiveID < 10)
//    cColor = gtxtTextures[4].Sample(gWrapSamplerState, input.uv);
//    else
//    cColor = gtxtTextures[5].Sample(gWrapSamplerState, input.uv);
//    */
//    float4 cColor = gtxtTextures[NonUniformResourceIndex(nPrimitiveID / 2)].Sample(gWrapSamplerState, input.uv);
//
//    return(cColor);
//}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//스카이박스 픽셀쉐이더

float4 PSSkyBox(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
    float4 cColor = gtxtSkyBox.Sample(gClampSamplerState, input.uv);

    return(cColor);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//텍스쳐드 인스턴싱 쉐이더

struct VS_TEXTURED_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float4x4 mtxTransform : WORLDMATRIX;
};

struct VS_TEXTURED_INSTANCING_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
};

VS_TEXTURED_INSTANCING_OUTPUT VSTexturedInstancing(VS_TEXTURED_INSTANCING_INPUT input)
{
    VS_TEXTURED_INSTANCING_OUTPUT output = (VS_TEXTURED_INSTANCING_OUTPUT)0;
    output.position = mul(mul(mul(float4(input.position, 1.0f), input.mtxTransform), gmtxView), gmtxProjection);
    output.color = float4(1.0f, 1.0f, 0.0f, 0.0f);
    output.uv = input.uv;
    return(output);
}

float4 PSTexturedInstancing(VS_TEXTURED_INSTANCING_OUTPUT input) : SV_TARGET
{
    input.color = gtxtTreeTexture.Sample(gWrapSamplerState, input.uv);
    //clip(input.color.a - 0.15f);
    return(input.color);
}