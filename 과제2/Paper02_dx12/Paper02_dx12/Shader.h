#pragma once

#include "Object.h"
#include "Camera.h"

//셰이더 소스 코드를 컴파일하고 그래픽스 상태 객체를 생성한다. 
class CShader
{
public:
    CShader() {};
    virtual ~CShader();
private:
    int m_nReferences = 0;
public:
    void AddRef() { m_nReferences++; }
    void Release() { if (--m_nReferences <= 0) delete this; }
    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
    virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
    virtual D3D12_BLEND_DESC CreateBlendState();
    virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob);
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);
    D3D12_SHADER_BYTECODE CompileShaderFromFile(WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob **ppd3dShaderBlob);
    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dRootSignature);
    
    void CreateCbvAndSrvDescriptorHeaps(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nConstantBufferViews, int nShaderResourceViews);
    void CreateConstantBufferViews(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nConstantBufferViews, ID3D12Resource *pd3dConstantBuffers, UINT nStride);
    void CreateShaderResourceViews(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, CTexture *pTexture, UINT nRootParameterStartIndex, bool bAutoIncrement);

    virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList); 
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList); 
    virtual void ReleaseShaderVariables();
    virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT4X4 *pxmf4x4World);
    virtual void ReleaseUploadBuffers();

    virtual void SetDescriptorHeaps(ID3D12GraphicsCommandList *pd3dCommandList);

    virtual void BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, void *pContext);
    virtual void AnimateObjects(float fTimeElapsed, CCamera * pCamera = NULL); 
    virtual void ReleaseObjects();
    virtual void OnPrepareRender(ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);

    D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart() { return(m_pd3dCbvSrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart()); }
    D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapStart() { return(m_pd3dCbvSrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart()); }

    D3D12_CPU_DESCRIPTOR_HANDLE GetCPUCbvDescriptorStartHandle() { return(m_d3dCbvCPUDescriptorStartHandle); }
    D3D12_GPU_DESCRIPTOR_HANDLE GetGPUCbvDescriptorStartHandle() { return(m_d3dCbvGPUDescriptorStartHandle); }
    D3D12_CPU_DESCRIPTOR_HANDLE GetCPUSrvDescriptorStartHandle() { return(m_d3dSrvCPUDescriptorStartHandle); }
    D3D12_GPU_DESCRIPTOR_HANDLE GetGPUSrvDescriptorStartHandle() { return(m_d3dSrvGPUDescriptorStartHandle); }
protected: 
    CGameObject ** m_ppObjects = nullptr;
    int m_nObjects;

    //파이프라인 상태 객체들의 리스트(배열)이다. 
    ID3D12PipelineState ** m_ppd3dPipelineStates = NULL; 
    int m_nPipelineStates = 0;

    ID3D12DescriptorHeap			*m_pd3dCbvSrvDescriptorHeap = NULL;

    D3D12_CPU_DESCRIPTOR_HANDLE		m_d3dCbvCPUDescriptorStartHandle;
    D3D12_GPU_DESCRIPTOR_HANDLE		m_d3dCbvGPUDescriptorStartHandle;
    D3D12_CPU_DESCRIPTOR_HANDLE		m_d3dSrvCPUDescriptorStartHandle;
    D3D12_GPU_DESCRIPTOR_HANDLE		m_d3dSrvGPUDescriptorStartHandle;
};

class CDiffusedShader : public CShader 
{
public: 
    CDiffusedShader(); 
    virtual ~CDiffusedShader();
    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob); 
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);
    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
#define _WITH_BATCH_MATERIAL

class CObjectsShader : public CShader 
{
public:    
    CObjectsShader();    
    virtual ~CObjectsShader();
    virtual void BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, void *pContext);
    virtual void AnimateObjects(float fTimeElapsed); 
    virtual void ReleaseObjects();

    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout(); 
    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob); 
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);

    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);

    virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void ReleaseUploadBuffers();

    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);
protected: 
  /*  CGameObject **m_ppObjects = NULL; 
    int m_nObjects = 0; */

    //ID3D12DescriptorHeap			*m_pd3dCbvSrvDescriptorHeap = NULL;

    ID3D12Resource					*m_pd3dcbGameObjects = NULL;
    UINT8							*m_pcbMappedGameObjects = NULL;

#ifdef _WITH_BATCH_MATERIAL
    CMaterial						*m_pMaterial = NULL;
#endif
};

/////////////////////////////////////////////////////////////////////////////////////

class CTerrainShader : public CShader 
{
public: 
    CTerrainShader(); 
    virtual ~CTerrainShader();
    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout(); 
    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob); 
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);
    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);

    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);
};

/////////////////////////////////////////////////////////////////////////////////////

struct CB_PLAYER_INFO
{
    XMFLOAT4X4						m_xmf4x4World;
};

class CPlayerShader : public CShader
{
public:
    CPlayerShader();
    virtual ~CPlayerShader();

    D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
    D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob ** ppd3dShaderBlob);
    D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob ** ppd3dShaderBlob);

    void CreateShader(ID3D12Device * pd3dDevice, ID3D12RootSignature * pd3dGraphicsRootSignature);

    virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void ReleaseShaderVariables();

    virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT4X4 *pxmf4x4World);

    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);
private:
    ID3D12Resource					*m_pd3dcbPlayer = NULL;
    CB_PLAYER_INFO					*m_pcbMappedPlayer = NULL;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CTexturedShader : public CShader
{
public:
    CTexturedShader();
    virtual ~CTexturedShader();

    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);

    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob);
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CSkyBoxShader : public CTexturedShader
{
public:
    CSkyBoxShader();
    virtual ~CSkyBoxShader();

    virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);

    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

//인스턴스 정보(게임 객체의 월드 변환 행렬과 객체의 색상)를 위한 구조체이다. 
struct VS_VB_INSTANCE
{
    XMFLOAT4X4 m_xmf4x4Transform;
    //XMFLOAT4 m_xmcColor;
};

class CInstancingShader : public CObjectsShader
{
public:
    CInstancingShader();
    virtual ~CInstancingShader();
    virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
    virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob **ppd3dShaderBlob);
    virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob **ppd3dShaderBlob);
    virtual D3D12_BLEND_DESC CreateBlendState();
    virtual void CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature);
    virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void ReleaseShaderVariables();
    virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList);
    virtual void BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, void *pContext);
    virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);

    virtual void AnimateObjects(float fTimeElapsed, CCamera * pCamera = NULL);

    void CreateInstancingBufferViews(void);

protected: //인스턴스 정점 버퍼와 정점 버퍼 뷰이다. 
    ID3D12Resource *m_pd3dcbGameObjects = NULL;
    VS_VB_INSTANCE *m_pcbMappedGameObjects = NULL;
    D3D12_VERTEX_BUFFER_VIEW m_d3dInstancingBufferView;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//