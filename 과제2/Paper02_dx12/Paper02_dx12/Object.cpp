#include "stdafx.h"
#include "Object.h"
#include "Shader.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CTexture::CTexture(int nTextures, UINT nTextureType, int nSamplers)
{
    m_nTextureType = nTextureType;
    m_nTextures = nTextures;
    if (m_nTextures > 0)
    {
        //CTexture는 SRV정보를 가지고 있는 배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_pRootArgumentInfos = new SRVROOTARGUMENTINFO[m_nTextures];
        //CTexture는 텍스쳐리소스업로드버퍼의 포인터배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_ppd3dTextureUploadBuffers = new ID3D12Resource*[m_nTextures];
        //CTexture는 텍스쳐정보의 포인터배열을 m_nTextures만큼 만들어서 멤버로 갖고있다.
        m_ppd3dTextures = new ID3D12Resource*[m_nTextures];
    }

    //우리는 staticsampler를 사용하기 때문에 여기는 안쓰일걸?
    m_nSamplers = nSamplers;
    if (m_nSamplers > 0) m_pd3dSamplerGpuDescriptorHandles = new D3D12_GPU_DESCRIPTOR_HANDLE[m_nSamplers];
}

CTexture::~CTexture()
{
    if (m_ppd3dTextures)
    {
        for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextures[i]) m_ppd3dTextures[i]->Release();
    }

    if (m_pRootArgumentInfos)
    {
        delete[] m_pRootArgumentInfos;
    }

    if (m_pd3dSamplerGpuDescriptorHandles) delete[] m_pd3dSamplerGpuDescriptorHandles;
}

void CTexture::SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSrvGpuDescriptorHandle)
{
    m_pRootArgumentInfos[nIndex].m_nRootParameterIndex = nRootParameterIndex;
    m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle = d3dSrvGpuDescriptorHandle;
}

void CTexture::SetSampler(int nIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSamplerGpuDescriptorHandle)
{
    m_pd3dSamplerGpuDescriptorHandles[nIndex] = d3dSamplerGpuDescriptorHandle;
}

void CTexture::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    if (m_nTextureType == RESOURCE_TEXTURE2D_ARRAY)
    {
        pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[0].m_nRootParameterIndex, m_pRootArgumentInfos[0].m_d3dSrvGpuDescriptorHandle);
    }
    else
    {
        for (int i = 0; i < m_nTextures; i++)
        {
            pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[i].m_nRootParameterIndex, m_pRootArgumentInfos[i].m_d3dSrvGpuDescriptorHandle);
        }
    }
}

void CTexture::UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, int nIndex)
{
    pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[nIndex].m_nRootParameterIndex, m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle);
}

void CTexture::ReleaseShaderVariables()
{
}

void CTexture::ReleaseUploadBuffers()
{
    if (m_ppd3dTextureUploadBuffers)
    {
        for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextureUploadBuffers[i]) m_ppd3dTextureUploadBuffers[i]->Release();
        delete[] m_ppd3dTextureUploadBuffers;
        m_ppd3dTextureUploadBuffers = NULL;
    }
}

void CTexture::LoadTextureFromFile(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, wchar_t *pszFileName, UINT nIndex)
{
    //CTexture는 텍스쳐(들)를 가지고 있다.
    m_ppd3dTextures[nIndex] = ::CreateTextureResourceFromFile(pd3dDevice, pd3dCommandList, pszFileName, &m_ppd3dTextureUploadBuffers[nIndex], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

CMaterial::CMaterial()
{
    m_xmf4Albedo = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);
}

CMaterial::~CMaterial()
{
    if (m_pShader)
    {
        m_pShader->ReleaseShaderVariables();
        m_pShader->Release();
    }
}

void CMaterial::SetShader(CShader *pShader)
{
    if (m_pShader) m_pShader->Release();
    m_pShader = pShader;
    if (m_pShader) m_pShader->AddRef();
}

void CMaterial::SetTexture(CTexture *pTexture)
{
    if (m_pTexture) m_pTexture->Release();
    m_pTexture = pTexture;
    if (m_pTexture) m_pTexture->AddRef();
}

void CMaterial::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
    if (m_pTexture) m_pTexture->UpdateShaderVariables(pd3dCommandList);
}

void CMaterial::ReleaseShaderVariables()
{
    if (m_pShader) m_pShader->ReleaseShaderVariables();
    if (m_pTexture) m_pTexture->ReleaseShaderVariables();
}

void CMaterial::ReleaseUploadBuffers()
{
    if (m_pTexture) m_pTexture->ReleaseUploadBuffers();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

CGameObject::CGameObject(int nMeshes) 
{
    m_xmf4x4World = Matrix4x4::Identity();
    m_nMeshes = nMeshes; 
    m_ppMeshes = NULL; 
    if (m_nMeshes > 0) 
    { 
        m_ppMeshes = new CMesh*[m_nMeshes]; 
        for (int i = 0; i < m_nMeshes; i++) 
            m_ppMeshes[i] = NULL; 
    }
}


CGameObject::~CGameObject()
{
    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->Release(); 
            m_ppMeshes[i] = NULL; 
        } 
        delete[] m_ppMeshes;
    }
    
    if (m_pShader)
    { 
        m_pShader->ReleaseShaderVariables(); 
        m_pShader->Release(); 
    }
}

void CGameObject::SetShader(CShader *pShader)
{
    //if (m_pShader) m_pShader->Release();
    //m_pShader = pShader;
    //if (m_pShader) m_pShader->AddRef();

    //이제부터는 메테리얼이 쉐이더를 갖는다.
    //만약 메테리얼이 없으면 바로 메모리할당한다음 쉐이더를 Set한다.
    if (!m_pMaterial)
    {
        CMaterial *pMaterial = new CMaterial();
        SetMaterial(pMaterial);
    }
    if (m_pMaterial) m_pMaterial->SetShader(pShader);
}

void CGameObject::SetMesh(int nIndex, CMesh *pMesh) 
{ 
    if (m_ppMeshes) 
    { 
        if (m_ppMeshes[nIndex]) m_ppMeshes[nIndex]->Release(); 
        m_ppMeshes[nIndex] = pMesh; 
        if (pMesh) pMesh->AddRef(); 
    } 
}

void CGameObject::SetMaterial(CMaterial *pMaterial)
{
    if (m_pMaterial) m_pMaterial->Release();
    m_pMaterial = pMaterial;
    if (m_pMaterial) m_pMaterial->AddRef();
}

void CGameObject::SetMaterial(UINT nReflection)
{
    if (!m_pMaterial) m_pMaterial = new CMaterial();
    m_pMaterial->m_nReflection = nReflection;
}

void CGameObject::ReleaseUploadBuffers()
{
    if (m_ppMeshes) 
    { 
        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->ReleaseUploadBuffers(); 
        } 
    }
}

void CGameObject::Animate(float fTimeElapsed, CCamera * pCamera) { }

void CGameObject::OnPrepareRender() { }

void CGameObject::Rotate(XMFLOAT3 *pxmf3Axis, float fAngle) 
{
    XMMATRIX mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(pxmf3Axis), XMConvertToRadians(fAngle)); 
    m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

void CGameObject::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList) 
{ 
    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    m_pd3dcbGameObject = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
    m_pd3dcbGameObject->Map(0, NULL, (void **)&m_pcbMappedGameObject);
}

void CGameObject::ReleaseShaderVariables() 
{ 
}

void CGameObject::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList) 
{
    XMStoreFloat4x4(&m_pcbMappedGameObject->m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
    m_pcbMappedGameObject->m_nMaterial = m_pMaterial->m_nReflection;
}

void CGameObject::SetDescriptorTable(ID3D12GraphicsCommandList * pd3dCommandList)
{
    pd3dCommandList->SetGraphicsRootDescriptorTable(0, m_d3dCbvGPUDescriptorHandle);
}

//1. 인스턴싱을 사용하지 않는방법
void CGameObject::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera) 
{
    OnPrepareRender();

    //이제부터 메테리얼이 쉐이더를 갖는다.
    if (m_pMaterial)
    {
        if (m_pMaterial->m_pShader)
        {
            m_pMaterial->m_pShader->Render(pd3dCommandList, pCamera);
            m_pMaterial->m_pShader->UpdateShaderVariables(pd3dCommandList);
            m_pMaterial->m_pShader->CShader::SetDescriptorHeaps(pd3dCommandList);
            //CGameObject::UpdateShaderVariables는 객체를 하나 그릴때만 사용된다.
            CGameObject::UpdateShaderVariables(pd3dCommandList);
        }

        if (m_pMaterial->m_pTexture)
        {
            m_pMaterial->m_pTexture->UpdateShaderVariables(pd3dCommandList);
        }
    }

    //현재 터레인과 구오브젝트들은 동일한 시그너쳐를 사용하고 있다.
    //그렇기 때문에 두 객체가 렌더링 될때마다 호출한다.
    //루트파라미터0번 인덱스에 있는 Descriptortable을 Set한다.
    //주의할점은 DescriptorTable은 DescriptorHeap이 우선적으로 Set되어있어야한다.
    CGameObject::SetDescriptorTable(pd3dCommandList);

    //게임 객체가 포함하는 모든 메쉬를 렌더링한다. 
    if (m_ppMeshes) 
    { 
        for (int i = 0; i < m_nMeshes; i++) 
        { 
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList); 
        } 
    } 
}

//2. 인스턴싱을 사용하는 방법
//인스턴싱 정점 버퍼 뷰를 사용하여 메쉬를 렌더링한다. 
void CGameObject::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera, UINT nInstances, D3D12_VERTEX_BUFFER_VIEW d3dInstancingBufferView)
{
    OnPrepareRender();
    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++)
        {
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList, nInstances, d3dInstancingBufferView);
        }
    }
}

void CGameObject::SetPosition(float x, float y, float z) 
{ 
    m_xmf4x4World._41 = x; 
    m_xmf4x4World._42 = y; 
    m_xmf4x4World._43 = z; 

}

void CGameObject::SetPosition(XMFLOAT3 xmf3Position) 
{ 
    SetPosition(xmf3Position.x, xmf3Position.y, xmf3Position.z); 
}

XMFLOAT3 CGameObject::GetPosition() 
{ 
    return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43)); 
}

//게임 객체의 로컬 z-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetLook() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33))); 
}

//게임 객체의 로컬 y-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetUp() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23))); 
}

//게임 객체의 로컬 x-축 벡터를 반환한다. 
XMFLOAT3 CGameObject::GetRight() 
{ 
    return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13))); 
}

//게임 객체를 로컬 x-축 방향으로 이동한다. 
void CGameObject::MoveStrafe(float fDistance) 
{
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Right = GetRight(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Right, fDistance); 
    CGameObject::SetPosition(xmf3Position); 
}

//게임 객체를 로컬 y-축 방향으로 이동한다. 
void CGameObject::MoveUp(float fDistance) 
{ 
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Up = GetUp(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Up, fDistance);
    CGameObject::SetPosition(xmf3Position);
}

//게임 객체를 로컬 z-축 방향으로 이동한다. 
void CGameObject::MoveForward(float fDistance) 
{ 
    XMFLOAT3 xmf3Position = GetPosition(); 
    XMFLOAT3 xmf3Look = GetLook(); 
    xmf3Position = Vector3::Add(xmf3Position, xmf3Look, fDistance); 
    CGameObject::SetPosition(xmf3Position); 
}

//게임 객체를 주어진 각도로 회전한다. 
void CGameObject::Rotate(float fPitch, float fYaw, float fRoll) 
{ 
    XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll)); 
    m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World); 
}

////////////////////////////////////

CRotatingObject::CRotatingObject(int nMeshes) : CGameObject(nMeshes) 
{ 
    m_xmf3RotationAxis = XMFLOAT3(0.0f, 1.0f, 0.0f); 
    m_fRotationSpeed = 15.0f; 

}

CRotatingObject::~CRotatingObject() 
{ 
}

void CRotatingObject::Animate(float fTimeElapsed, CCamera * pCamera)
{ 
    CGameObject::Rotate(&m_xmf3RotationAxis, m_fRotationSpeed * fTimeElapsed); 
}


//////////////////////////////////////////////////////////////////////////


CRotatingMoveObject::CRotatingMoveObject(int nMeshes) : CRotatingObject(1)
{
    m_xmf3Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_fMovingSpeed = 0.0f;
    m_pTerrainData = NULL;
}

CRotatingMoveObject::~CRotatingMoveObject()
{
    delete m_pTerrainData;
}

void CRotatingMoveObject::SetDirection(XMFLOAT3 xmf3Direction)
{
    m_xmf3Direction = xmf3Direction;
    m_xmf3Direction = Vector3::Normalize(m_xmf3Direction);
}

void CRotatingMoveObject::SetMovingSpeed(float fMovingSpeed)
{
    m_fMovingSpeed = fMovingSpeed;
};

void CRotatingMoveObject::SetTerrainData(CHeightMapTerrain * pTerrain)
{
    m_pTerrainData = pTerrain;
}

XMFLOAT3 CRotatingMoveObject::GetDirection(void)
{
    return m_xmf3Direction;
}

void CRotatingMoveObject::Animate(float fTimeElapsed, CCamera * pCamera)
{
    assert(m_pTerrainData != NULL && "Terrain데이터가 없습니다");

    Move();
    CGameObject::Rotate(&m_xmf3RotationAxis, m_fRotationSpeed * fTimeElapsed);

   
}

void CRotatingMoveObject::Move()
{
    //객체가 터레인 밖으로 나가면 방향벡터가 반사된다.
    CheckTerrainOutSide();

    //객체의 높이값을 터레인에 맞춰서 조정한다.
    UpdateByTerrainData();
   
    XMFLOAT3 xmf3Position = Vector3::Add(GetPosition(), m_xmf3Direction, m_fMovingSpeed);
    CGameObject::SetPosition(xmf3Position);
}

void CRotatingMoveObject::UpdateByTerrainData(void)
{
    XMFLOAT3 xmf3Position = GetPosition();

    float fHeight = m_pTerrainData->GetHeight(xmf3Position.x, xmf3Position.z) + 10.0f;

    //객체y 값이 터레인 y값보다 작으면 올린다.
    if (xmf3Position.y < fHeight)
    {
        xmf3Position.y = fHeight;
        SetPosition(xmf3Position);
    }
    //객체y 값이 터레인 y값보다 크면 내린다.
    else if (xmf3Position.y > fHeight)
    {
        xmf3Position.y = fHeight;
        SetPosition(xmf3Position);
    }
}

XMFLOAT3 CRotatingMoveObject::CheckTerrainOutSide(void)
{
    XMFLOAT3 xmf3Position = GetPosition();
    
    XMFLOAT3 xmf3Normal;
    if (xmf3Position.x > m_pTerrainData->GetWidth()) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(-1.0f, 0.0f, 0.0f));
    if (xmf3Position.x < 0) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(1.0f, 0.0f, 0.0f));
    if (xmf3Position.z > m_pTerrainData->GetLength()) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(0.0f, 0.0f, -1.0f));
    if (xmf3Position.z < 0) return m_xmf3Direction = Vector3::Reflect(m_xmf3Direction, XMFLOAT3(0.0f, 0.0f, 1.0f));

    //터레인 밖으로 나가지 않으면 기존 방향벡터를 반환
    return m_xmf3Direction;
}


///////////////////////////////////////////////////////////////////////


CHeightMapTerrain::CHeightMapTerrain(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, 
    ID3D12RootSignature *pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength, 
    int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color) : CGameObject() 
{ 
    //지형에 사용할 높이 맵의 가로, 세로의 크기이다. 
    m_nWidth = nWidth; 
    m_nLength = nLength;
    
    /*지형 객체는 격자 메쉬들의 배열로 만들 것이다. nBlockWidth, nBlockLength는 격자 메쉬 하나의 가로, 세로 크 기이다. cxQuadsPerBlock, czQuadsPerBlock은 격자 메쉬의 가로 방향과 세로 방향 사각형의 개수이다.*/ 
    int cxQuadsPerBlock = nBlockWidth - 1; 
    int czQuadsPerBlock = nBlockLength - 1;
    
    //xmf3Scale는 지형을 실제로 몇 배 확대할 것인가를 나타낸다. 
    m_xmf3Scale = xmf3Scale;
    
    //지형에 사용할 높이 맵을 생성한다. 
    m_pHeightMapImage = new CHeightMapImage(pFileName, nWidth, nLength, xmf3Scale);
    
    //지형에서 가로 방향, 세로 방향으로 격자 메쉬가 몇 개가 있는 가를 나타낸다. 
    long cxBlocks = (m_nWidth - 1) / cxQuadsPerBlock; 
    long czBlocks = (m_nLength - 1) / czQuadsPerBlock;

    //지형 전체를 표현하기 위한 격자 메쉬의 개수이다. 
    m_nMeshes = cxBlocks * czBlocks; 
    
    //지형 전체를 표현하기 위한 격자 메쉬에 대한 포인터 배열을 생성한다. 
    m_ppMeshes = new CMesh*[m_nMeshes]; 
    for (int i = 0; i < m_nMeshes; i++)
        m_ppMeshes[i] = NULL;

    CHeightMapGridMesh *pHeightMapGridMesh = NULL; 
    for (int z = 0, zStart = 0; z < czBlocks; z++) 
    {
        for (int x = 0, xStart = 0; x < cxBlocks; x++) 
        { 
            //지형의 일부분을 나타내는 격자 메쉬의 시작 위치(좌표)이다. 
            xStart = x * (nBlockWidth - 1); 
            zStart = z * (nBlockLength - 1); 
            //지형의 일부분을 나타내는 격자 메쉬를 생성하여 지형 메쉬에 저장한다. 
            pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, pd3dCommandList, xStart, zStart, 
                nBlockWidth, nBlockLength, xmf3Scale, xmf4Color, m_pHeightMapImage); 
            SetMesh(x + (z*cxBlocks), pHeightMapGridMesh); 
        } 
    }
                     
    //터레인정보를 담고 있는 리소스를 만들고 Map함수를 통해서 리소스 주소값을 가져온다.
    CreateShaderVariables(pd3dDevice, pd3dCommandList); 

    CTerrainShader * pTerrainShader = new CTerrainShader();
    pTerrainShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);

    //디스크립터힙을 만들고, cpu,gpu 디스크립터 핸들을 얻는다. 여기서는 cbv한개와 srv2개를 담을 디스크립터힙를 생성한다.
    pTerrainShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 2);

    //디스크립터힙을 만들었으니 힙안에다가 view를 만든다. 
    //힙안에 cbv 1개 생성
    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
    pTerrainShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, m_pd3dcbGameObject, ncbElementBytes);
    
    CTexture *pTerrainTexture = new CTexture(2, RESOURCE_TEXTURE2D, 0);
    pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/Terrain/Base_Texture.dds", 0);
    pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/Terrain/Detail_Texture_7.dds", 1);

    //힙안에 srv 2개 생성
    pTerrainShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pTerrainTexture, 6, true);
  
    CMaterial * pTerrainMaterial = new CMaterial();

    //머테리얼은 재질(빛을 반사하는 성질)을 가지고 있다.
    pTerrainMaterial->SetReflection(1);

    //머테리얼은 텍스쳐를 가지고 있다.
    pTerrainMaterial->SetTexture(pTerrainTexture);

    //머테리얼은 쉐이더도 가지고 있다.
    pTerrainMaterial->SetShader(pTerrainShader);

    //터레인오브젝트는 재질을 가지고있다.
    //SetShader를 호출하려면 먼저 메테리얼이 Set 되있어야할것이다. 왜냐? 상위 개념이기때문에
    //고로 SetMaterial이 SetShader보다 먼저 호출되어야함.
    //결국 터레인 오브젝트는 머테리얼이라는 변수를 가지고 있는데 
    //그 머테리얼은 텍스쳐와 재질(빛을 반사하는 성질), 쉐이더를 가지고 있다.
    SetMaterial(pTerrainMaterial);

    //터레인오브젝트는 힙의 gpu첫주소값을 쉐이더로부터 얻는다.
    SetCbvGPUDescriptorHandle(pTerrainShader->GetGPUCbvDescriptorStartHandle());

    //여기서 pTerrainShader 메모리를 해제하면 안될것이다. 왜냐? ExecuteCommandList가 아직 호출이 안됐기때문에.
}

CHeightMapTerrain::~CHeightMapTerrain(void) 
{ 
    if (m_pHeightMapImage) delete m_pHeightMapImage; 
}

void CHeightMapTerrain::Render(ID3D12GraphicsCommandList * pd3dCommandList, CCamera * pCamera)
{
    CGameObject::Render(pd3dCommandList, pCamera);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
CSkyBox::CSkyBox(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature) : CGameObject(6)
{
    CTexturedRectMesh *pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, +10.0f);
    SetMesh(0, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, -10.0f);
    SetMesh(1, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 0.0f, 20.0f, 20.0f, -10.0f, 0.0f, 0.0f);
    SetMesh(2, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 0.0f, 20.0f, 20.0f, +10.0f, 0.0f, 0.0f);
    SetMesh(3, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 0.0f, 20.0f, 0.0f, +10.0f, 0.0f);
    SetMesh(4, pSkyBoxMesh);
    pSkyBoxMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, 20.0f, 0.0f, 20.0f, 0.0f, -10.0f, 0.0f);
    SetMesh(5, pSkyBoxMesh);

    //아래 함수를 통해서 스카이박스정보를 저장할수있는 리소스를 만들고 Map함수를 통해서 리소스 주소값을 가져온다.
    CreateShaderVariables(pd3dDevice, pd3dCommandList);

    CTexture *pSkyBoxTexture = new CTexture(6, RESOURCE_TEXTURE2D, 0);

    //텍스쳐(dds)를 불러온다
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Front_0.dds", 0);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Back_0.dds", 1);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Left_0.dds", 2);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Right_0.dds", 3);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Top_0.dds", 4);
    pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Assets/Image/SkyBox/SkyBox_Bottom_0.dds", 5);

    UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

    //스카이박스는 쉐이더를 가지고 있다.
    CSkyBoxShader *pSkyBoxShader = new CSkyBoxShader();
    pSkyBoxShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
    //pSkyBoxShader->CreateShaderVariables(pd3dDevice, pd3dCommandList);
    //스카이박스쉐이더는 cbv,srv를 binding하는 descriptorHeap을 생성한다. cbv 1개, srv 6개
    pSkyBoxShader->CreateCbvAndSrvDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 6);
    //위에서 만든 descriptorHeap에 cbv1개를 Set한다.
    pSkyBoxShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, m_pd3dcbGameObject, ncbElementBytes);
    //위에서 만든 descriptorHeap에 srv(6개)를 Set한다. 마지막인자가 False는 하나의 루트파라미터인덱스를 사용한다는 의미
    pSkyBoxShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pSkyBoxTexture, 2, false);

    //재질은 텍스쳐를 가지고 있다.
    CMaterial *pSkyBoxMaterial = new CMaterial();

    pSkyBoxMaterial->SetTexture(pSkyBoxTexture);

    pSkyBoxMaterial->SetShader(pSkyBoxShader);

    //여기서 만약 재절(빛을 반사하는성질)을 따로 Set해주지않으면 디폴트 재질이 생성되서 Set된다?

    //스카이박스 오브젝트는 머테리얼(m_pMaterial)을 가지고 있다.
    //그 머테리얼은 재질(빛을 반사하는 성질)과 텍스쳐와 쉐이더를 가지고 있다.
    SetMaterial(pSkyBoxMaterial);

    //스카이박스 오브젝트는 힙의 gpu첫주소값을 쉐이더로부터 얻는다.
    SetCbvGPUDescriptorHandle(pSkyBoxShader->GetGPUCbvDescriptorStartHandle());
}

CSkyBox::~CSkyBox()
{
}

void CSkyBox::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
    XMFLOAT3 xmf3CameraPos = pCamera->GetPosition();
    SetPosition(xmf3CameraPos.x, xmf3CameraPos.y, xmf3CameraPos.z);

    OnPrepareRender();

    if (m_pMaterial)
    {
        if (m_pMaterial->m_pShader)
        {
            m_pMaterial->m_pShader->Render(pd3dCommandList, pCamera);
            m_pMaterial->m_pShader->UpdateShaderVariables(pd3dCommandList);
            m_pMaterial->m_pShader->CShader::SetDescriptorHeaps(pd3dCommandList);
            UpdateShaderVariables(pd3dCommandList);
        }
    }

    CGameObject::SetDescriptorTable(pd3dCommandList);

    if (m_ppMeshes)
    {
        for (int i = 0; i < m_nMeshes; i++)
        {
            if (m_pMaterial)
            {
                if (m_pMaterial->m_pTexture) m_pMaterial->m_pTexture->UpdateShaderVariable(pd3dCommandList, i);
            }
            if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CBillBoardObject::CBillBoardObject() : CGameObject(1)
{
}

CBillBoardObject::~CBillBoardObject()
{
}

void CBillBoardObject::Animate(float fTimeElapsed, CCamera * pCamera)
{
    XMFLOAT3 xmf3CameraPosition = pCamera->GetPosition();
    SetLookAt(xmf3CameraPosition);
}

void CBillBoardObject::SetLookAt(XMFLOAT3 & xmf3Target)
{
    XMFLOAT3 xmf3Up(0.0f, 1.0f, 0.0f);
    XMFLOAT3 xmf3Position(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43);
    XMFLOAT3 xmf3Look = Vector3::Subtract(xmf3Target, xmf3Position);
    xmf3Look = Vector3::Normalize(xmf3Look);
    XMFLOAT3 xmf3Right = Vector3::CrossProduct(xmf3Up, xmf3Look, true);
    m_xmf4x4World._11 = xmf3Right.x; m_xmf4x4World._12 = xmf3Right.y; m_xmf4x4World._13 = xmf3Right.z;
    m_xmf4x4World._21 = xmf3Up.x, m_xmf4x4World._22 = xmf3Up.y; m_xmf4x4World._23 = xmf3Up.z;
    m_xmf4x4World._31 = xmf3Look.x; m_xmf4x4World._32 = xmf3Look.y; m_xmf4x4World._33 = xmf3Look.z;
}


